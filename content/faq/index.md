---
title: "Frequently Asked Questions"
date: 2021-09-12
---

#### Where is this website hosted?
This website is generated using Gitlab Pages and hosted on Hetzner. check out [my friend's blog post on the same](https://blog.sahilister.in/2021/11/updates-hosting-moved-and-nginx-logs/index.html) for details. 

#### Where did you purchase the domain from?
I purchased the domain from gandi.net.

#### How much does the domain costs you?
USD 10 per year.

#### Where are your emails hosted?
gandi.net

#### If I visit your website, do you know about the visit, i.e., does the website contain any trackers or analytics?
No. The website has no trackers or analytics and I do not know if anyone is visting the website or not. You can check using ublock origin or Privacy Badger. These add-ons usually tell you if there are any analytics used by the websites that you visit. Further, there is no Javascript code used on the website.  
