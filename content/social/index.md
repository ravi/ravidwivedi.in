---
title: "My Online Profiles"
date: 2024-02-27T15:44:26+05:30
type: "page"
showTableOfContents: true
draft: false
---
List of my online profiles

- Mastodon/Fediverse: [@ravi@toot.io](https://toot.io/@ravi).

- Diaspora: [ravi on poddery.com](https://poddery.com/people/c9137600b8b201390011448a5bd87df2).

- [Movim](https://chat.macaw.me/contact/ravi%40macaw.me).

- [Pixelfed](https://pixel.tchncs.de/ravi).

- [gitlab.com](https://gitlab.com/ravidwivedi).

- [salsa.debian.org](https://salsa.debian.org/ravi).

- Codeberg - [ravidwivedi](https://codeberg.org/ravidwivedi).

- Peertube - [videos.fsci.in](https://videos.fsci.in/accounts/ravidwivedi/video-channels).
