---
title: "Contact"
date: 2021-07-12
type: "page"
showTableOfContents: true
---
## Email

My email address is: `ravi at ravidwivedi dot in`. For the privacy of our communication, I recommend you encrypt all your emails to me. Here's an [easy to follow guide](https://www.zdnet.com/article/how-to-encrypt-your-email-and-why-you-should/) for beginners. In case you choose to send a GPG encrypted email to me, my GPG keys are [here](/gpg).

## XMPP

[ravi@durare.org](https://join.jabber.network/#ravi@durare.org) is my XMPP address. Easiest way to use XMPP is to install Quicksy on Android. If you are curious, learn more at [joinjabber.org](https://joinjabber.org/). 

## Matrix

[@ravi:poddery.com](https://matrix.to/#/@ravi:poddery.com).

Scanning the QR code below will add me on matrix.

<center><img src="/images/matrix-qr-code.jpg" width="300"></center>

## IRC  

ravi on irc.libera.chat

## Mastodon/Fediverse

<a rel="me" href="https://toot.io/@ravi">@ravi@toot.io</a>
