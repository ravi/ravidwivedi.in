---
title: "About me"
date: 2021-10-14
draft: false
---
Hello, welcome to my blog. My name is Ravi and I share my stories and ideas on this blog. I hope that keeps you engaged and entertained. 

Education wise, I am from mathematical background. I have studied BSc Honors in Mathematics from [Acharya Narendra Dev College](https://www.andcollege.du.ac.in/), New Delhi and M.Math from [Indian Statistical Institute, Kolkata](https://en.wikipedia.org/wiki/Indian_Statistical_Institute). I work as a freelancer for [artofproblemsolving.com](https://artofproblemsolving.com)

I am a Free Software activist and use it exclusively for all my needs. A few years ago, I realized the need of challenging the monopology of WhatsApp in messaging. Therefore, I am a part of the [Prav project](https://prav.app), of which I am currently serving as the Chief Executive (CEO).

Further, I [package](https://qa.debian.org/developer.php?login=Ravi%20Dwivedi%20%3Cravi@anche.no%3E) for [Debian](https://debian.org), run [mirrors](https://mirrors.ravidwivedi.in) for LibreOffice and Termux, and map for [OpenStreetMap](https://openstreetmap.org). I like traveling and exploring places.

I can't wait to share my [blog posts](/posts) with you!

<figure>
<img loading="lazy" src="/images/Me-at-Taj-Mahal.JPG" style="height:400px;"> 
<figcaption><center>Location: Taj Mahal, Agra, India.</center></figcaption>
</figure>
