---
title: "About me"
date: 2021-10-14
draft: false
---
Hello, I’m Ravi and I work as a freelancer for [artofproblemsolving.com](https://artofproblemsolving.com). I am currently the Chief Executive (CEO) of the [Prav](https://prav.app) cooperative. 

I have studied BSc Honors from [Acharya Narendra Dev College](https://www.andcollege.du.ac.in/), New Delhi and M.Math from [Indian Statistical Institute, Kolkata](https://en.wikipedia.org/wiki/Indian_Statistical_Institute).

I also run [mirrors](https://mirrors.ravidwivedi.in) for LibreOffice and Termux. I like traveling and exploring places. Furthermore, I often find myself reading psychology :)

I can't wait to share my [blog posts](/posts) with you!

<figure>
<img loading="lazy" src="/images/Me-at-Taj-Mahal.JPG" style="height:400px;"> 
<figcaption><center>Location: Taj Mahal, Agra, India.</center></figcaption>
</figure>
