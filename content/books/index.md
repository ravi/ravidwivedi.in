---
title: "Book Suggestions"
date: 2021-09-06
type: "post"
showTableOfContents: true
---
<b>I have collected some quotes from these books on a <a href="/book-quotes">separate page</a>.</b>

### Fiction

My favorite authors are Margaret Atwood, George Orwell, Kazuo Ishiguro, Phillip K. Dick.

- [The Remains of the Day](https://en.wikipedia.org/wiki/The_Remains_of_the_Day) by Kazuo Ishiguro: A literary masterpiece.

- [A Clockwork Orange by Anthony Burgess](https://en.wikipedia.org/wiki/A_Clockwork_Orange_(novel)) : This raises a very important moral question, "Is it better to choose to be bad rather than being conditioned to do only good?"

- [Nineteen Eighty-Four by George Orwell](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four): A dystopian world where every move of citizens is watched by authorities. Without spoiling too much, I would like to recommend it as it is one of my favorites.

- [Things Fall Apart](https://en.wikipedia.org/wiki/Things_Fall_Apart) by Chinua Achebe: It is based in Nigeria in colonial times and I got to know a bit about Igbo people and their traditions.

- [Klara and the Sun by Kazuo Ishiguro](https://en.wikipedia.org/wiki/Klara_and_the_Sun): There is something about the narrators in Kazuo's novel that i like. They can be pretty unreliable. In this one, the narrator is an AI and so it is interesting to see her perspectives and understanding of humans.

- [Cat's Cradle by Kurt Vonnegut](https://en.wikipedia.org/wiki/Cat%27s_Cradle): A humourous book mocking scientists and religion. I like Vonnegut's way of storytelling.

- [Half of a Yellow Sun](https://en.wikipedia.org/wiki/Half_of_a_Yellow_Sun) by Chimamanda Ngozi Adichie: It is a beautiful novel which tells stories of lives of people during the [Nigerian Civil War](https://en.wikipedia.org/wiki/Nigerian_Civil_War) through the characters of five people.

- [The Handmaid's Tale by Margaret Atwood](https://en.wikipedia.org/wiki/The_Handmaid%27s_Tale): Margaret Atwood's creativity and the dystopian world she had imagined really freaked me out here.

- [A Thousand Splendid Suns](https://en.wikipedia.org/wiki/A_Thousand_Splendid_Suns) by Khaled Hosseini: Khaled Hosseini shows the power of love through a story based in Afghanistan, a country torn apart with continuous conflict since 1978, and the Taliban rule turns the worse situation into a disaster. This is one of the saddest books I have ever read. If you want to know the situation of Afghanistan, go for it. The beginning is set in 1964 and the story ends in 2003, we see regimes switching hands so many times, with drastically different fates of women.

- [Flowers for Algernon](https://en.wikipedia.org/wiki/Flowers_for_Algernon) by Daniel Keyes: Heartbreaking. It is on how society treats mentally retarded. I wasn't satisfied with some parts of the book, but overall a creative one and I was instantly drawn to it. 

- [Oryx and Crake by Margaret Atwood](https://en.wikipedia.org/wiki/Oryx_and_Crake): This is a dystopian novel which is just so close to reality. With the technology growing up and tech corporations dominating the world, how will the future turn out for us?

- [To Kill a Mockingbird by Harper Lee](https://en.wikipedia.org/wiki/To_Kill_a_Mockingbird) : A hypocritic society seen through the lens of children of around 8 years old. 

- [We Need New Names](https://en.wikipedia.org/wiki/We_Need_New_Names) by NoViolet Bulawayo: Another book told through eyes of a child.

- [A Tale for the Time Being by Ruth Ozeki](https://en.wikipedia.org/wiki/A_Tale_for_the_Time_Being) : Ruth Ozeki finds diary of a Japanese girl, which she suspected came from 2011 Tsunami to the shore in Canada. She gets worried about the safety of the girl while reading through the diary and a tragic tale unfolds. I didn't like the spiritual or "quantum mechanics" elements in the book.

- [Fight Club by Chuck Palahniuk](https://en.wikipedia.org/wiki/Fight_Club_(novel)): On the surface it seems like a critical look on our capitalistic lifestyles. But I feel like it is about fascism.

- [Animal Farm by George Orwell](https://en.wikipedia.org/wiki/Animal_Farm): Wanna know how democracy turns into a dictatorship? Pick up this.

- [The Murder of Roger Ackroyd by Agatha Christie](https://en.wikipedia.org/wiki/The_Murder_of_Roger_Ackroyd) : Very good plot. If I tell you anything about a detective novel like this, it will spoil the novel for you. 

### Nonfiction

Richard Dawkins is one of my favourites among the nonfiction writers.

- [How Democracies Die](https://en.wikipedia.org/wiki/How_Democracies_Die) by Steven Levitsky and Daniel Ziblatt: The book explores subversion of democracy by elected leaders themselves, known as [democratic backsliding](https://en.wikipedia.org/wiki/Democratic_backsliding).

- [Thinking, Fast and Slow by Daniel Kahneman](https://en.wikipedia.org/wiki/Thinking%2C_Fast_and_Slow): If you have never read anything in psychology, this will be a good start.

- [When Crime Pays: Money and Muscle in Indian Politics](https://yalebooks.yale.edu/book/9780300216202/when-crime-pays/) by Milan Vaishnav: It attempts to explain the supply and demand of visibly criminal candidates in the Indian democracy.

- [Invisible Women: Exposing Data Bias in a World Designed for Men](https://en.wikipedia.org/wiki/Invisible_Women:_Exposing_Data_Bias_in_a_World_Designed_for_Men) by Caroline Criado Perez.

- [The Selfish Gene](https://en.wikipedia.org/wiki/The_Selfish_Gene) by Richard Dawkins: Book on evolutionary biology explaining several types of human behaviour in terms of theory of selfish gene.

- [Price of the Modi Years](https://en.wikipedia.org/wiki/Price_of_the_Modi_Years) by Aakar Patel.

- The Silent Coup: A History of India’s Deep State by Josy Joseph.

- [Annihilation of Caste](https://en.wikipedia.org/wiki/Annihilation_of_Caste) by Ambedkar.

- [Phantoms in the Brain](https://en.wikipedia.org/wiki/Phantoms_in_the_Brain) by V. S. Ramachandran.

- [No Place to Hide](https://en.wikipedia.org/wiki/No_Place_to_Hide_%28Greenwald_book%29) by Glenn Greenwald.

<br>
