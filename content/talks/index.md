---
title: "My Talks"
date: 2021-07-29
type: "page"
showTableOfContents: true
---
## A beginner's guide to debian packaging

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/debconf23-300-a-beginners-guide-to-debian-packaging.av1.webm" frameborder="0" allowfullscreen></iframe>

<details>
	<summary>Details</summary>
<b>Speaker(s):</b> Ravi Dwivedi  
<br>
<b>Language:</b> English
<br>
<b>Event:</b> <a href="https://debconf23.debconf.org/">DebConf 23</a>.
<br>
<b>Slides:</b> <a href="https://salsa.debian.org/debconf-team/public/share/debconf23/-/blob/main/slides/A-beginner_s-guide-to-debian-packaging.pdf">PDF</a>
<br>
<b><a href="https://debconf23.debconf.org/talks/81-a-beginners-guide-to-debian-packaging/">Talk Description</a>:</b>  This talk is intended for complete beginners. Even those unfamiliar to debian can attend it. This talk will emphasize what it means to package, why packaging is an important contribution and demonstrate the process of packaging using beginner friendly examples. I will first setup a debian sid environment for packaging and then demonstrate packaging examples in that environment. It can act as a video document for beginners to start learning debian packaging. The talk will also point to material on debian packaging for further reading and how to get involved with the community for contributing to packaging work.

Important links:

- https://wiki.debian.org/Packaging/Pre-Requisites
- https://wiki.debian.org/Packaging/Learn
- https://wiki.debian.org/BuildingTutorial

</details>

## Prav: a community-owned chat app

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.fsci.in/videos/embed/7268cae2-218c-4692-a7ac-84102799746c" frameborder="0" allowfullscreen></iframe>

<details>
        <summary>Details</summary>
	
**Date:** 11 March 2023  
**Speaker(s):** Ravi Dwivedi, Arun Mathai  
**Language:** English  
**Event:** <a href="https://indiafoss.net/Mumbai/2023">MumbaiFOSS</a>.  
**Video Link:** [Official Prav channel](https://videos.fsci.in/videos/watch/7268cae2-218c-4692-a7ac-84102799746c) | [Official FOSSUnited Channel](https://piped.video/watch?v=_QAgPVhHyIE).  
<b>Slides:</b> <a href="https://codeberg.org/prav/prav-assets/raw/branch/main/presentation-mdc-tn23/Presentation.pdf">PDF</a>
<br>
</details>

## Importance of Free Software in Education

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.fsci.in/videos/embed/1c6f69c4-0842-4112-9e7b-2df489505aea" frameborder="0" allowfullscreen></iframe>

<details>
        <summary>Details</summary>

**Speaker(s):** Ravi Dwivedi  
**Language:** Hindi   

</details>

## Interview by LibreOffice

[Link to interview](https://blog.documentfoundation.org/blog/2021/12/13/community-member-monday-ravi-dwivedi/).

<details>
        <summary>Details</summary>

**Date:** 13 December 2021  
**Speaker(s):**   
**Language:** English   

</details>

## Freedom-respecting Software, Free Society

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.fsci.in/videos/embed/0562cbc4-02bb-41a2-abfe-6ceb9994f2da" frameborder="0" allowfullscreen></iframe>

<details>
        <summary>Details</summary>

**Date:** 20 November 2021  
**Speaker(s):** Ravi Dwivedi  
**Language:** English   
**At:** [Department of Scientific Computing, modeling and simulation](https://scms.unipune.ac.in/), Savitribai Phule Pune University, Pune, India.
</details>

## An Introduction to Free Software Communication Tools

<iframe title="An Introduction to free software communication tools [Hindi]" src="https://peertube.debian.social/videos/embed/2b21c084-9522-4c8c-98c7-985c06d3b9d5" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

<details>
        <summary>Details</summary>

**Date:** 23 January 2021  
**Speaker(s):** Ravi Dwivedi  
**Language:** Hindi   
**Event:** [MiniDebconf India 2021](https://in2021.mini.debconf.org/)  

</details>

## BoF: Digital privacy in Indian context and some Free software

<iframe title="BoF: Digital privacy in Indian context and some Free software tools and services to the rescue [Hindi]" src="https://peertube.debian.social/videos/embed/417bd62c-cce1-494a-b96c-b0e39741cb09" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

<details>
        <summary>Details</summary>

**Date:** 23 January 2021  
**Speaker(s):** Ravi Dwivedi, Sahil Dhiman, Rojin Roy, Akshay S Dinesh, Riya Sawant
**Language:** Hindi   
**Event:** [MiniDebconf India 2021](https://in2021.mini.debconf.org/)  

</details>
