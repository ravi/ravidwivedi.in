---
date: 2023-07-18
type: "page"
showTableOfContents: true
draft: false
---
Hi, My name is Ravi. I write on this website in my free time and I find it fun to share knowledge and opinion with others. You will find myself raising awareness about different topics on this site, mainly about the [Free Software Movement](https://en.wikipedia.org/wiki/Free_software_movement). I like to travel and explore new places, especially food. I am from a beautiful country called India, which has endless diversity and exciting places to visit. 

