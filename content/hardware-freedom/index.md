---
title: "Hardware Freedom"
date: 2021-07-20
type: "page"
showTableOfContents: true
---
*Last Updated: 20 October 2022* 

<b> Disclaimer: The info here can be outdated or can be factually incorrect. Please verify the facts yourself. [Contacting me](/contact) and reporting issues will be highly appreciated. </b>

You want to run [free software](/free-software)? That's a good step towards your freedom and privacy. But where will you run it? You must have hardware which supports free software. Is your hardware capable of running only on free software? If not (for example, Macbook or iPhone are not capable of running fully free software), then next time, consider buying hardware which can run on free software. Here I have curated a list of shops and places where you can get them. And also which products can run free software. There might be other shops/hardware I don't know about. Please let me know if you have any suggestions.

### Hardware which can run exclusively on Free Software

This section lists hardware or projects which develop hardware which run exclusively on [Free Software](/posts/free-sw). The ideal case scenario is the hardware should not require any proprietary software to operate. In some examples, we might not be able to achieve this ideal case scenario but actually come very close to achieving it. 

We need to fund the development of such hardware which can run exclusively on Free Software. 

A lot of hardware can run Free Software applications like Operating System and software we use in daily life. The problems arise in finding hardware which can run without requiring [proprietary firmware](https://en.wikipedia.org/wiki/Proprietary_firmware) like BIOS and without requiring the use of backdoor like Intel ME. Before we jump ahead to listing freedom-respecting hardware, let's get introduced to these problems briefly.

#### Problems

- Intel Management Engine: Intel Management Engine is a backdoor which can be used by Intel to remotely access the computers running Intel ME. [Quoting Libreboot project on Intel ME](https://libreboot.org/faq.html#intelme):

	"The Intel Management Engine with its proprietary firmware has complete access to and control over the PC: it can power on or shut down the PC, read all open files, examine all running applications, track all keys pressed and mouse movements, and even capture or display images on the screen. And it has a network interface that is demonstrably insecure, which can allow an attacker on the network to inject rootkits that completely compromise the PC and can report to the attacker all activities performed on the PC. It is a threat to freedom, security, and privacy that can’t be ignored."

	Check out [the article on Intel ME backdoor by FSF](https://www.fsf.org/blogs/sysadmin/the-management-engine-an-attack-on-computer-users-freedom).

- Nonfree firmware: [Read this article](/posts/firmware).

Shops which sell hardware which can run exclusively free software:

- [LibreTech Shop](https://libretech.shop/) by [Mostly Harmless](https://mostlyharmless.io). Mostly Harmless is a shop based in India which sells [liberated laptops](https://liberated.computer), [liberated phones](https://mostlyharmless.io/phones/), [a USB drive which can automatically detect iso files and flash them](https://mostlyharmless.io/usb/), [keyboards](https://mostlyharmless.io/keyboards/), [keypads](https://mostlyharmless.io/keypads/), [home automation systems](https://mostlyharmless.io/home-automation/), [routers](https://mostlyharmless.io/routers/). Each of these hardware respect your freedom by running free software in it.

- [Purism](https://puri.sm).

- [Minifree project](https://minifree.org/).

Below are some good sources/list of devices that can run exclusively free software:

- [h-node](https://h-node.org/): The h-node project aims at the construction of a hardware database in order to identify what devices work with a fully free operating system. The h-node.org website is structured like a wiki in which all the users can modify or insert new contents. The h-node project is developed in collaboration and as an activity of the FSF.

- [RYF certified devices by FSF](https://ryf.fsf.org/). 

- [List of phones which support Replicant](https://www.replicant.us/supported-devices.php).

- [Debian's article on purchasing hardware specifically for GNU/Linux](https://www.debian.org/releases/stable/amd64/ch02s03.en.html#idm351)

### Hardware with GNU/Linux preinstalled

These laptops may require nonfree drivers for wifi and other hardware to work. If you want laptops with GNU/Linux preinstalled, these are very good choices because the company also provides support.

- Lenovo sells [laptops which have Ubuntu preinstalled](https://news.lenovo.com/pressroom/press-releases/lenovo-launches-linux-ready-thinkpad-and-thinkstation-pcs-preinstalled-with-ubuntu/) and also [Fedora preinstalled](https://fedoramagazine.org/lenovo-fedora-now-available/).

- Clevo is shipping laptops [out-of-the-box with Debian](https://laptopwithlinux.com/buy-debian-laptop-notebooks-with-debian-linux-preinstalled/) worldwide. Debian is a [very secure operating system](https://lists.debian.org/debian-security/2022/04/msg00047.html) for laptops.

- [A list of computers](https://manjaro.org/hardware/) which ship Manjaro preinstalled.

- [Tuxedo computers](https://www.tuxedocomputers.com/en) ship with GNU/Linux preinstalled. 
