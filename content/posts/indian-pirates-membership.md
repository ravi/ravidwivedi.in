---
title: "Announcement: Became a Member of Indian Pirates"
draft: false
type: "post"
date: 2022-03-01
showTableOfContents: true
---
I am happy to announce that I became a permanent member of [Indian Pirates](https://pirates.org.in) a few days ago. Indian Pirates is a decentralized group which aims to be a political party someday and contest elections to form a government. I hope to bring some change politically to improve our lives. The group seeks long term change which means that we are not thinking that we will change the world in day or in one year, but rather understanding the roots of the problems in society and how it is structured and solve the problems by the root itself.

It is only an announcement post and I want to keep it short. Good Bye. And see you in the next post.
