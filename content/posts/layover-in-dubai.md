---
title: "My Layover in Dubai"
date: 2022-07-31T13:19:11+05:30
draft: false
type: "post"
tags: ["travel", "dubai", "uae"]
showTableOfContents: true
---
After a nice Debconf in Kosovo, I was returning to Delhi, India on a FlyDubai flight from Tirana, capital of Albania, with a layover in Dubai for a bit over 24 hours. I was joined by my friend [Abraham Raji](https://abrahamraji.in), who had to go to Kochi, and had a layover of around 22 hours. Before booking the tickets, I was told by my travel agent that I needed a transit visa for staying more than 24 hours at the Dubai Airport. This visa also allows you to exit the airport and roam around the city during the layover. Abraham also had the transit visa, though he didn't need it to take the flight.

After my flight landed at the terminal 2 of the Dubai airport, I went through immigration and customs, and got seated in the arrivals area at around 22:00 hours local time. Soon Abraham joined me. 

Dubai Airport has a very good Wi-Fi which was a life saver, be it searching information, getting entertained or calling home, it never disappointed. The Wi-Fi has free unlimited access. You just need to simply select the “DXB Free Wifi” option and it connects without asking for a phone number or OTP.

At one point, I remember Abraham talking to a man who recommended us to withdraw at least 200 Dirhams (around 4,000 INR) in cash from ATM in order to roam around the city, which I considered to be a high amount even after considering Dubai being a very expensive city. Spoiler Alert: I didn't even spend 50 Dirhams roaming around in this layover.

We planned to spend the night at the airport and roam around in the city in the morning. We didn’t book a hotel and instead planned to sleep on the benches in the arrivals. There were no airport lounges and cloak room. I was hungry as I didn't had lunch due to the lack of lacto-ovo vegetarian options on the flight and so, I took a banana from the Costa Coffee shop, which was inside the arrivals, for 6 AED (equivalent to 120 Indian Rupees). Very expensive!

Unable to sleep, we came out of the airport in search of a place to eat. It was already midnight and all shops were closed. We were suggested by someone to check whether the McDonald's inside the departures section was open. I was not sure whether we can just go inside the departures and come back into the city as this is not allowed at Indian airports. Then an Indian claimed a restaurant to be open near the Al-Qiyada metro station, which was 2 kilometer walk from the airport. We walked there and didn’t find any restaurant open, so had to return with disappointment. We made some interesting conversations on the way to pass the time.

Upon returning to the airport, we saw a restaurant open by the name of 'Food Castle Express' within 1 km of the arrivals section. The restaurant had Indian food with reasonable prices. It was being run by Malayalis and seemed a popular place among the airport staff. To give you an idea of prices, a plate of 3 Idlis was 5.5 AED (equivalent to 110 Indian Rupees), a cup of tea was 1 AED (20 Indian Rupees), a plate of Pav Bhaji was 8 AED (around 160 Indian Rupees), while Chicken Samosa, Aloo Samosa and Cheese Samosa, were 1.5 AED per piece, and Chicken Fried Rice was 12 AED.

I took the following:
| Item                  | Price (AED) | Price (INR) |
|-----------------------|-------------:|-------------:|
| Idli (3 pcs)          |          5.5|          110 |
| 1 Tea            	|          1  |           20 |
| Chhole Bhature        |          7.5|          150 |
| **Total**             |        **14**|         **280** |

We had this meal around 04:30 hours in the morning and it kept me satiated literally for the whole day. We got to eat Indian food after a 3-week Europe trip. After eating, we went back to the Arrivals section to catch some sleep. Abraham took a nap on the benches, while I failed to get any. In the meanwhile, I found out that the water inside the washroom was too hot to even wash my face. 

At 09:00 hours, we exited the airport and were looking for a bus to drop us at some metro station to visit the Burj Khalifa and Dubai Mall. We found out that a Nol card is required to use public transport(buses, trams, metro) in Dubai and metro stations is one place to obtain them.

The nearest metro station was Abu Hail Metro station, 2 km walk from the airport, which was not exactly fun as Dubai has a hot desert climate and the temperature was around 40°C, with sand coming into my eyes. To add to his, we had a lot of luggage. At the Abu Hail metro station, we took the silver card which was 25 AED (500 INR) with 19 AED balance.

The metro from Abu Hail to Burj Khalifa was 5 AED, which was much cheaper than taking a taxi, which from the airport, would have added 25 AED as the basic fare. We reached the Burj Khalifa metro station in 25 minutes and walked to Burj Khalifa, clicked some photos. It was the tallest building in the world, so clicking photos wasn't so easy. 

{{< figure
  src="/images/dubai/pic-with-abraham-at-burj-khalifa.jpg"
  alt="Two people looking towards the viewer"
  link="/images/dubai/pic-with-abraham-at-burj-khalifa.jpg"
  caption="Me and Abraham with Burj Khalifa in the background"
>}}

After roaming around a bit, we went to the Dubai Mall, which was walking distance from the Burj Khalifa. The strategy was to spend as much time as possible inside such buildings and malls to avoid the harsh weather. Abraham had a meal in the mall, while I didn’t eat anything due to lack of vegetarian options.

{{< figure
  src="/images/dubai/dubai-mall.jpg"
  alt=""
  link="/images/dubai/dubai-mall.jpg"
  caption="Dubai Mall"
>}}

We still had 14 AED left in our Nol card and we wanted to spend that amount before leaving the city. So, we planned a tram ride which will spend almost all the card balance, cover the city and does not require walking outside of air-conditioning. To take the tram, we went to Sobha Realty Metro Station and walked towards the tram station.
 
The tram ride was nice. The passengers need to be careful to check-in and check-out using the Nol card at the tram station, otherwise they could be fined. We saw the Dubai’s Marina area from the tram. The tram takes 3 AED regardless of the station you check out from. After the tram ride, we took metro from DMCC station and returned to Abu Hail metro station, followed by walking towards the airport, planning to have a meal at the restaurant we dined at in the morning. I didn't find a good vegetarian option there, so I skipped eating there and just took a chai.

{{< figure
  src="/images/dubai/tram-in-dubai.jpg"
  alt=""
  link="/images/dubai/tram-in-dubai.jpg"
  caption="View from inside the tram"
>}}

After this, we returned to the airport to catch our respective flights. There was no language problem for us in Dubai. English was widely spoken. Most of my conversations was in Hindi as I met many Indians and Pakistanis who knew Hindi. In fact, Abraham was talking to people in Malayalam. Overall, people were very nice which made us feel like home.

The weather was harsh and it was tiring but anyways it was a fun layover in Dubai which I did just within 50 AED, which is equivalent to 1000 INR.
