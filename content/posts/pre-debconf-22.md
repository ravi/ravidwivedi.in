---
title: "Complicated Kosovo Visa"
tags: ["debconf", "debian", "travel", "visa", "kosovo"]
date: 2022-07-13T18:04:50+02:00
draft: false
showTableOfContents: false
type: "post"
tags: ["visa", "kosovo", "debconf", "debconf22"]
---
DebConf is the annual conference of the [Debian](/posts/what-is-debian) community. [This year's DebConf](https://debconf22.debconf.org/) was held in Kosovo, a European country from the Balkans.

<figure>
<img src="https://wiki.debian.org/DebConf/22/Organizers?action=AttachFile&do=get&target=debconflogo22l.png" width="300">
<figcaption>DebConf22 logo</figcaption>
</figure>

Debian sponsored me for the conference. Indian attendees needed a visa to enter Kosovo, which got complicated due to Kosovo not having any embassies in India. To obtain a visa, we could travel to some other country and visit a Kosovo embassy to apply for a visa. However, it was not practical. Another option was to send our passports to a Kosovo embassy in another country by post. Sending my passport outside the country seemed risky and time consuming. Fortunately, the organizing team found another way - by sending the required documents by email to Kosovo embassy in Tirana, Albania and collecting the visas by visiting the embassy in person. This was possible because Albania was [visa-free for Indians](https://punetejashtme.gov.al/en/regjimi-i-vizave-per-te-huajt/) during that time period. However, this is not the standard way to get a Kosovo visa and it was an exception made by the Ministry of Foreign Affairs of Kosovo for us.

The conference was to begin on the 10th of July. So, I booked my flight tickets from Delhi to Tirana for the 6th and bus tickets from Tirana to Prizren for the 10th of July. Further, I transferred the visa fee worth 40 Euros into the embassy's bank account using wise.com. On the 9th of June, I emailed my documents to the Kosovo embassy in Tirana for my visa application. Here is the list of documents I sent:

- Filled and signed Visa Application form 

- Scanned copy of my passport 

- 1 photo of myself

- Invite letter and proof of travel, food and accommodation bursary.

- Confirmed return flight ticket from Delhi to Tirana

- Bus tickets from Tirana to Prizren

- Bank statement (last 3 months)

- Health insurance valid throughout the territory of the Republic of Kosovo

- Receipt of payment of 40 Euros for visa fee

I did not receive any acknowledgement of the receipt of my email from the embassy. After not receiving a response, I wrote an email to the embassy asking for my visa application status on the 22nd of June. The embassy responded by saying that my name is not in the list of approved names.

> Your name is not in the list of approved names to apply remotely. Therefore your case cannot be processed.

A couple of other Indian attendees reported the same response from the embassy. However, Utkarsh from the bursary team confirmed that our names were in the list. On the 27th of June, with 9 days to go before our flight to Albania, Praveen (a Debian Developer) sent an email to the Debian Project Leader Jonathan Carter, CCing all the prominent DebConf team members and Indian attendees, sharing our frustation with the process. He proposed Debian to reimburse tickets if we don't get a response from the embassy. On the other hand, Arianit Dobroshi from the organizing team was fairly confident about the approval and there was no need to panic. In the same email, Praveen also proposed Debian to include visa fee in the bursary by default for the sponsored attendees, for which Jonathan shared his views in a public mailing list [here](https://lists.debian.org/debian-project/2022/06/msg00020.html).

As the travel date was approaching, I was panicking. I asked my travel agent to check on the cancelation fee for my flight, which was around 15,000 INR. However, repeated assurances from Arianit made us stay on our plans. On the 3rd of July, he sent us a letter from Kosovo's Ministry of Foreign Affairs stating that our visas will be processed in Tirana, Albania. The problem is that at the airport, the airlines will ask you for the Kosovo visa if you say you are going to Kosovo. If you cannot present a Kosovo visa, they can choose not to give you your boarding pass. So, the document stating that our applications were being processed in Tirana was not enough.

My flight to Tirana was on the 6th July early morning. I was boarding alone from Delhi, while there were six attendees boarding from Kochi and two from Mumbai the same day. So, I reached the Delhi airport around 3-4 hours before the flight's departure time. My airline was Flydubai. As soon as I went to their counter for the boarding pass, they checked my passport and asked where I was going. I told them I was going to Albania. They asked me to show my visa for Albania. I told them that Albania was visa free. They checked and this was correct. They asked the purpose of my visit to which I responded "conference". This followed by them asking me to show the invitation letter, which I did. However, the invitation letter mentioned the venue of the conference as Kosovo. 

To my surprise, the airline did not ask me for a Kosovo visa even though the invitation letter clearly mentioned Kosovo. My guess is that they didn't notice. Is it possble they don't know about Kosovo as India doesn't recognize it? They asked me to show all the hotel bookings (since DebConf covered my accommodation, I only showed hotel bookings for dates before and after the conference). Then they asked further questions: about my college degree, work, how much currency I was carrying, etc. The interrogation made me feel like I was a criminal. At one point, I had a feeling that they wil deny boarding to me.

After grilling me for some time, the airline gave me my boarding pass. The next stop was immigration. However, the immigration only asked me where I was going, to which I said "Albania" and they asked me to show visa. I told them that Albania was visa-free. So, they only checked whether Indians require a visa to visit Albania. I was elated after the immigration was cleared. This followed by boarding the flight.

I had a connecting flight from Dubai. Here I met other DebConf attendees coming from Kochi and Mumbai. Before boarding the flight, we were asked once again on our purpose to visit Albania to which we showed our invitation letters. Again, they didn't notice the conference was in Kosovo.

On the 7th of July, while we were in Tirana, we received the following email from one of the conference organizers, Arianit:

> For Tirana applicants,
> 
> We got notification that visas have arrived in Tirana. Please show up at
> the Consulate tomorrow at 08:00 to get them stamped. They work until 13:30
> tomorrow. We are trying to find a solution for people arriving in Tirana at
> 14:00 apparently.
> 
> Regards,
> Arianit

Next day, on the 8th of July, we went to the Kosovo embassy early in the morning and got our visas. This followed by a scenic morning bus trip from Tirana to Prizren, which was the city DebConf was held in.

<figure>
<img src="/images/itp-prizren.jpg" width="500">
<figcaption>The Innovation & Training Park (ITP) in Prizren, Kosovo was the venue of DebConf 22.</figcaption>
</figure>

ITP Prizren was the venue of DebConf. It was surrounded by mountains and te views here were treat for my eyes. To me, it seemed like a miracle to be here in Prizren attending DebConf. Looking forward to work on some projects and meeting nice people.
