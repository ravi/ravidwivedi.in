---
title: "Book Review: Phantoms in the Brain"
date: 2021-09-09
draft: false
type: "post"
showTableOfContents: true
---

A few days ago, I finished reading the book [Phantoms in the Brain](https://en.wikipedia.org/wiki/Phantoms_in_the_Brain) by [V. S. Ramachandran](https://en.wikipedia.org/wiki/V._S._Ramachandran). The topic covered here is fascinating, interesting, mysterious, disturbing and shocking. The cliche "Reality is stranger than fiction" comes to mind.

The book covers many disorders and tries to explain them in concrete naeurological terms. 

One of the cases presented in the book was that majority of amputees who suddenly lost a limb in an accident feel pain in the lost limb. Many patients feel that the fingers of the lost limb (fingers are not even attached to the body) are clenched tightly and this leads to unbearable pain (called [phantom pain](https://en.wikipedia.org/wiki/Phantom_pain) and hence the title of the book). 

In the past, neurologists regarded such cases as mental problems and handed over the patient to psychiatrists. The book holds the view that due to damages in specific portions of the brain leads to loss of functionality which that portion carries. This is fascinating because when trying to understand these cases, it seems like we are only looking at exceptional cases, but at the same time, they do tell a lot about the normal human brain. Ramachandran brought a fresh appraoch and looked at how phantoms (the patients still feel that the lost limb is attached to the body and such a limb is called a phantom limb) are generated and how the brain can be tricked into unlearning a phantom. Would you like to look at the solution/treatment of the phantom pain? [It is here](https://en.wikipedia.org/wiki/Mirror_therapy). The solution is interesting as well. 

Human brain can easily be tricked. Our brains carry so many [cognitive biases](https://en.wikipedia.org/wiki/List_of_cognitive_biases), delusions, denial of the obvious facts. Quoting Oliver Sacks from the preface of the book, "The deeply strange business of mirror agnosia, and that of misattri­buting one's own limbs to others, are often dismissed by physicians as irrational. But these problems are also considered carefully by Rama­chandran, who sees them not as groundless or crazy, but as emergency defense measures constructed by the unconscious to deal with sudden
overwhelming bewilderments about one's body and the space around it. They are, he feels, quite normal defense mechanisms ( denial, repression, projection, confabulation, and so on) such as Freud delineated as universal strategies of the unconscious when forced to accommodate the intolerable or unintelligible." 

The topic presented here aligns very well with [Daniel Kahneman's Thinking, Fast and Slow](https://en.wikipedia.org/wiki/Thinking,_Fast_and_Slow), which says that there are two modes of thinking -- System 1 and System 2. System 1 is fast, effortless and jumps to conclusions. It cannot be consciously controlled by us but we can change our habits to feed them into System 1 (like driving, walking, playing a cricket ball are in System 1 of the person practicing enough). System 2 is slow, effort and resource consuming. Heavy cognitive tasks are done using System 2. 

Overall, Phantoms in the brain urges neuroscientists to be more open in their approach, while at the same time being fun to read, engaging and accessible to the general audience.
