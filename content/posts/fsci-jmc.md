---
title: "My experience in volunteering for FSCI's Jitsi Meet Crowdfunding campaign"
date: 2021-07-21
---

showTableOfContents: true
[Free Software Community of India(FSCI)](fsci.in) is committed to promote software freedom and make privacy accessible to common people. Campaigning and raising awareness for privacy and urging people to not use Google/Facebook/Zoom and other proprietary services is not enough, users must also have alternatives to these services which were pretty much turned into necessity (for privileged people) especially in the pandemic time. Jitsi is a [freedom-respecting](http://www.gnu.org/philosophy/free-software-even-more-important.html) video-conferencing software which gives users the control over the software. Jitsi allows anyone to run their own server, allowing them to fully control their means of communication. Since it is not feasible for everyone to run their own Jitsi server, FSCI decided to work on running their own server which would be open to all, can be used by anyone for their meetings, funded by voluntary donations and must respect user's privacy. 

To run a server funded by donations, we needed volunteers for: running and maintaining a Jitsi server, writing script for the fundraiser video, asking for donations in the fundraiser video, editing the fundraiser video, maintaining website, giving a bank account to accept donations, replying to emails regarding queries, taking initiative and coordinating all this. 

I think it was around November 2020 when [Sahil](https://blog.sahilister.in/) told me about FSCI's initiative to run a Jitsi server and I agreed to volunteer for asking for donations in the fundraiser video. [Arun Mathai](https://arunmathaisk.in/) guided me in the process. We had meetings and we rehearsed the script a few times and assigned the lines. I had some problems shooting the video of myself speaking in the camera. Talking to myself alone on camera is not exactly my plus point. After several attempts, I could make a clip which was fine to be included in the fundraiser video (thanks to mom for shooting the clip). 

We thought it would be good if there is diversity among the people asking for donations in the fundraiser video. So, I asked my friend Ashutosh, who is also inclined towards free software, if he can help. Then he discussed the fundraiser with his friends and two of his friends volunteered to appear in the video to ask for doantions. This opportunity raised awareness about free software and existence of community-powered services among his friends -- which furthers our goal. I also asked my friend Sayan for recording the Bengali snippet of the video and he agreed to volunteer for the same. 

It took some months after that (we did all of the above by the end of December 2020) to finally release the website and the Jitsi server acutally operational. After the release, we, at FSCI, use meet.fsci.in for all our meetings. I use this server for all my personal meetings.

 I am glad that the fundraiser is going strong -- as of writing, we have raised [57% of the funds](https://fund.fsci.in/contributors/) needed to run the service for 2 years. As of now, I think that drafting the privacy policy is a pending major task which I will try to do as soon as possbile.

You are invited to use the Jitsi server at [meet.fsci.in](https://meet.fsci.in) for your personal meeting needs without worrying about someone prying on you. You can also host your classes. The server can handle around 30 participants in a meeting. If you would like to support the service, you can donate by visitng [here](https://fund.fsci.in) or volunteer to help us in running the service by [contacting FSCI](https://fsci.in). The transparency is ensured by listing all the contributors and their donation amounts publicly (you can be an anonymous contributor).

Please send your comments/suggestions on the fundraiser/meet server to FSCI (the contact option is at the bottom of the [link](https://fund.fsci.in/). 

Thanks to all the volunteers and [contributors](https://fund.fsci.in/contributors/) for making [meet.fsci.in](https://meet.fsci.in) possible.
