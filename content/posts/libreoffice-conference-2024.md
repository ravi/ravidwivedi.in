---
title: "My Experience of Attending Libreoffice Conference 2024 in Luxembourg"
date: 2025-01-23T18:12:17+05:30
draft: true
type: "post"
tags: ["libreoffice", "conference", "europe", "luxembourg"]
showTableOfContents: false
---
Last year, I attended the annual LibreOffice Conference in Luxembourg with the help of a generous travel grant by The Document Foundation (TDF). It was a three-day event from the 10th to the 12th of October, with an additional day for community meetup on the 9th.

Luxembourg is a small (twice as big as Delhi) country in Western Europe. After going through an arduous visa process, I reached Luxembourg on the 8th of October. Upon arriving in Luxembourg, I took a bus to the city center, where my hotel — Park Inn — was located. All the public transport in Luxembourg was free of cost. It was as if I stepped in another world. There were separate tracks for cycling and a separate lane for buses, along with good pedestrian infrastructure. In addition, the streets were pretty neat and clean.

My hotel was 20 km from the conference venue in Belval. However, the commute was convenient due to a free of cost train connection, which were comfortable, smooth, and scenic, covering the distance in half an hour. The hotel included a breakfast buffet, recharging us before the conference.

Pre-conference, a day was reserved for the community meetup on the 9th of October. On that day, the community members introduced themselves and their contributions to the LibreOffice project. It acted as a brainstorming session. I got a lovely conference bag, which contained a T-Shirt, a pen and a few stickers. I also met my long time collaborators Mike, Sophie and Italo from the TDF, whom I had interacted only remotely till then. Likewise, I also met TDF's sysadmin Guilhem, who I interacted before regarding setting up my LibreOffice mirror.

The conference started on the 10th. There were 5 attendees from India, including me, while most of the attendees were from Europe. The talks were in English. One of the talks that stood out for me was about Luxchat — a chat service run by the Luxembourg government based on the Matrix protocol for the citizens of Luxembourg. I also liked Italo's talk on why document formats must be freedom-respecting. On the first night, the attendees went for a nice conference dinner in a restaurant.

On the 11th of October, I went for a walk in the morning with Biswadeep for some sightseeing. As a consequence, I missed the group photo of the conference. Anyway, we enjoyed roaming around the picturesque Luxembourg city. We took a tram ride to return to our hotel.

The conference ended on the 12th with a couple of talks. This conference gave me an opportunity to meet the global LibreOffice community, connect and share ideas. It also gave me a peek into the country of Luxembourg and its people, where I had good experience. English was widely known, and I had no issues getting by.
