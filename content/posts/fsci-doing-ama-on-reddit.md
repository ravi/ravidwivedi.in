---
title: "Free Software Community of India will Do 'Ask Me Anything' on Reddit"
date: 2022-04-09T02:01:36+05:30
draft: false
type: "post"
showTableOfContents: true
---
Free Software Community of India [will be doing](https://fsci.in/blog/fsci-ama-reddit/) an 'Ask Me Anything' session on r/India subreddit. One of the moderators of the r/India reached out to us via filling the contact form on the website as well as in our chat group. As soon as I saw that the person filled the form, I replied it by email. It took some time for the community to arrive at a date and time for conducting the AMA, but now it is finalized to be 12 April 2022 17:00 IST.  

Akshay created a Reddit account for FSCI and shared the credentials. Me, Praveen, Akshay and Arya will be answering the AMA. Hopefully, this will raise awareness about [Free Software](/free-software) in India and the existence of our community. Our community is always in the 'Ask us Anything' mode because of our welcoming nature and our active lookout for new people to get involved. So if you miss the AMA due to some reason, then you can just ask us in our [chat group](https://fsci.in/#join-us). AMA is just one more way to ask us questions.

So, tune into r/India subreddit on 12 April at 5 PM IST. Ask me anything. See you there.
