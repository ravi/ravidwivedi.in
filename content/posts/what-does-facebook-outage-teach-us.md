---
title: "What Does The Facebook Outage Teach Us"
draft: false
type: "post" 
date: 2021-11-25
showTableOfContents: true
---

(Note: Facebook does not have users, it has useds.)

On 4th October 2021, [Facebook, Instagram, WhatsApp were down](https://www.nytimes.com/2021/10/04/technology/facebook-down.html) for more than 6 hours. This had a huge impact on many of its useds, [with devastating effect to many](https://webfoundation.org/2021/11/when-facebook-goes-down-whats-an-inconvenience-to-some-is-devastating-to-others/). During the outage, many useds flocked to [Twitter](https://www.heraldscotland.com/news/19624807.twitter-users-report-outage-facebook-whatsapp-instagram-go/), [Discord](https://downdetector.com/status/discord/), [Signal, and Telegram](https://www.bloomberg.com/news/articles/2021-10-05/millions-flock-to-signal-as-facebook-whatsapp-suffer-outage), resulting in disruptions on these apps' servers. Many businesses, which rely on Facebook's services for their business, [were down](https://www.nytimes.com/2021/10/04/technology/facebook-down.html). In many countries, Facebook is synonymous with the internet and therefore their [communications, business, payments, humanitarian work got disrupted](https://news.trust.org/item/20211005204816-qzjft/). The outage temporarily broke the ability for some Facebook employees to [access company buildings and conference rooms with their badges](https://www.theverge.com/2021/10/4/22709575/facebook-outage-instagram-whatsapp). And every third-party site that relies on “log in with Facebook” didn't work as well.

What does this teach us? --- That Facebook's services are centralized, which means that one entity controls them. They are single point of failure and control. A single company like Facebook controlling a large part of communications is no different than a dictator. You are at a mercy of them. 

This tells us that the whole world relying on a single company for all their communications, businesses etc. is not sustainable. 

In addition, such a service can be sold to any other company, like [WhatsApp was sold to Facebook](http://newsroom.fb.com/News/805/Facebook-to-Acquire-WhatsApp). So, even if the service is good today, it can become bad in the future-- like, by selling it to another company or [by changing privacy policy](https://www.macrumors.com/2021/01/06/whatsapp-privacy-policy-data-sharing-facebook/). Also, it is easier for governments to ban such a service.

[Facebook is so bad for many reasons](https://stallman.org/facebook.html) that my advice to you is to delete your Facebook account.

Now, what can we do about it? 

I do not use or be used by any centralized service, be it Signal, Twitter, Telegram. I use decentralized and federated services. Email is an example of federation. A @gmail.com user can write mails to @yahoo.com user and vice-versa. This gives a choice of service providers, and therefore the entire communication systems of the world do not depend entirely on one service. If one service goes down, then this won't disrupt the communications of the whole world. 

Examples of decentralized networks are : Jitsi Meet(a video-calling software), Searx search engine etc. They can be self-hosted by anyone in their server. This gives users a choice of service providers. 

Examples of federated networks are: Fediverse, XMPP, Matrix etc. 

XMPP is a chat protocol. There are many services which a user can register with and still talk to another XMPP user who is registered on another XMPP service. This choice of service providers ensures that users are not locked into a single provider. 

To get started with XMPP, you can use Quicksy app on Android. Using Quicksy is no different than WhatsApp or Signal, and it is very convenient. The difference being Quicksy users can talk to users registered on other XMPP providers. This ensures there is no lock-in to a single provider. If one service on XMPP shuts down or changes their policy, users can switch to other provider without going to the trouble of convincing every contact to switch to a new service. Compare this with WhatsApp, Signal, Telegram. If they change their policy or you disagree with these providers in future, you need to convince every contact to switch to that new service. 

<b>The bottom line is that Facebook's outage is a reminder that they are a single point of failure, and other centralized services are no different in this aspect. Switch to decentralized and federated services instead, rather than being locked into a single provider for all your needs.</b> 

