---
title: "How I got into Free Software"
date: 2022-01-07
draft: false
type: "post"
showTableOfContents: true
---

Before I forget my journey, I hastily record it here.

It is a long series of triggers and questions raised in my mind after [I met Richard Stallman in 2016](/posts/meeting-rms/) which were responsible for this.

I thought on the issues that he raised but didn't act for years. Occasionally, thoughts about WhatsApp spying on me came to mind. Thoughts about Google recording my whole life. I told one of my friends about Facebook surveillance, and they said they have nothing to hide. Another friend I told about Uber tracking us. Another friend I told about the music app tracking us, and they responded by saying that they used it for their benefit by getting recommendations related to their music taste. All this time, I didn't think hard enough. I merely knew about tracking, and I gave up my rights in exchange for convenience.

But many of the times, there were search results logged by Google which I didn't want to share with them. The question was what should I do? What should anyone do to browse the web who doesn't want to sign some arbitrary terms and conditions set up by a company who is not even accountable for their actions. So, I used a proprietary VPN app from the Play Store to "protect" myself. But Google still showed the search results in my profile. Now I really don't want to tell Google where I am or what I search for, but on the other hand, services like Google search and Google Maps do give convenience. Also, not having a single person sharing this view in my immediate friends circle also discouraged me constantly, even if I wanted to do something about it. 

In this time period, I also saw some ads by DuckDuckGo on Quora which constantly reminded me about me being pissed off with Google and their surveillance. Thanks, DuckDuckGo :)

Now, in June 2020, my master's degree got over, and I got some time to think about the issue. I took the challenge. And as it was, it started with this challenge. I thought switching a little software or using Tor or Brave will give me privacy. I thought I will probably do it in 2-3 days. As it turned out, that thought was really stupid! The so called Dunning-Kruger effect was at work, which says that beginners in a field of study tend to overestimate their knowledge.

Initially it started as a challenge and not a quest to change the world or anything. Then I stared searching in DuckDuckGo and felt happy about it. It felt cool because nobody I knew had DuckDuckGo as their default search engine. I also used to search Reddit for information on the issue. I came to know about browser fingerprinting technique used by websites like Google to track users. As depressing as it was, using a VPN does not magically make the user invisible and there were so many complexities involved in making decisions on how to be private or anonymous. 

I came across Glenn Greenwald's book 'No Place To Hide' and I read it fully which was about Snowden's revelations of massive surveillance by NSA. I felt so much for Snowden. His courage also inspired me. He risked his life to tell the world about NSA's secret spying programs. Whenever I talked to someone about the surveillance, they seemed reluctant to change or even admit that there is some threat. And I thought that this person named Snowden risked his whole life by standing for us against the powerful US Government, but we cannot even compromise on a few conveniences for our privacy. 

I didn't had a framework to think on how to go about this. I wanted digital privacy but it is a complicated thing. I didn't even think at that time on a threat model. I also used to think 'What if the replacement services or software are spying on me?'. How do I verify?

This is where I took a page from Richard Stallman's book, perhaps, I took many pages. It occured to me that 'if we cannot even inspect what the software does, how can we be sure that the software respects privacy?' I realized that users do not control a nonfree software. I took some time but I got convinced about the [free software being necessary but not sufficient for privacy](/posts/free-software-important-for-privacy/). So, the replacements I started using, like VLC Media Player or other software, were free software and this was a big difference. Now I had an answer to why use this software and how is it different. At least I was not trusting blindly on the software. I also realized that all the freedoms of free software are important and privacy is not the only issue.

At the time, I didn't had any idea on how to think about messenger apps or search engines or anything server-based which you do not even install in your own computer. 

By September 2020, I already boycotted most of the software/services by Google, Amazon, Microsoft, Facebook and started using Free Software as much as possible. Back then, I had a Macbook so boycotting Apple would take more time. In September 2020, I came to know about FSF India and immediately wrote a volunteer request to FSF India. I wanted to meet like-minded people from India. Till that point, I was doing things all alone, by myself. After a few days, Pirate Praveen replied my volunteer mail. So that is how I joined the community and get in touch with others. I also participated in [Software Freedom Camp 2020](https://camp.fsf.org.in) where I got to meet more people from the free software movement.

In the camp, I met many people. [Sahil](https://sahilister.in) and [Arun](https://arunmathaisk.in) were in my group and we had meetings late night to discuss about privacy and teaching each other. In the later stage, I met Akhil, Anupa, Bady, [Akshay](https://blog.learnlearn.in). And then [Karthik](https://kskarthik.gitlab.io/) who taught some Hugo and command line to me. 

After interaction with the community, I realized that centralization of services is another problem and decentralization, federation of services is necessary for user's freedom, in addition to free software. That enhanced my thinking, and now I could think on '[Freedom in the cloud](https://fsci.in/blog/self-hosting-and-federation/)'. I also realized how free software community works in practice. Earlier, I only knew the philosophy of free software.

Fast-forward to April 2021, I left FSF India due to [their support for Richard Stallman at the event of him being re-elected as a board member of FSF](https://fsf.org.in/news/board-statement-2021/). After that, I focused on campaigning for free software on behalf of [Free Software Community of India(FSCI)](https://fsci.in). I became an activist by this time. I think the main reason I became an activist is that you cannot use Free Software or have privacy/anonymity in isolation. Whatever the majority of people use is usually forced upon the minority. Free Software users who care for philosophy of free software are in minority, and therefore we have to constantly fight, for example, against the social pressure of using WhatsApp. I also realized that the real way to solve the problem is raising awareness on the issue and help people in switching to Free Software. User boycott of nonfree software is a powerful way to defeat the companies who exploit users for using their services. 

Naturally, I wanted to switch to fully free software, which means not using any proprietary software. [I bought a Liberated Computer](/posts/liberated-computer/) which can run exclusively free software, thanks to Abhas. Abhas also taught me how to install a custom ROM in the phone so that I can get rid of proprietary apps. Now I could do my computing without giving away freedom, which is a unique accomplishment in today's world. This way I switched to free software and my Macbook passed on to someone else who seems happy to be imprisoned by it(but I am not happy that I did this to someone).

Thanks to all the people who made it possible. First of all, huge thanks to my parents for cooperating (to an extent). Without them supporting me, it wouldn't have been possible. There are many people who took time and effort to teach me many things. Also, there were already technical solutions for freedom. Free Software movement has existed for long. Thanks to all the people for their contributions to the software I rely on. Thanks to the Free Software Community.

I would like to take the opportunity to say that we do not really lack in technical solutions for these problems, but rather the willpower of people to use them.

In the end, I would like to say that the journey has not ended, it has only started.
