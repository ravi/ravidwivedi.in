---
title: "How I choose my distros"
draft: false
type: "post" 
date: 2021-12-07 
showTableOfContents: true
---

If you don't know what a distro is, I will explain that in a minute.

First, I would like to ask you a question

Which operating system do you use? 

Do you use Microsoft Windows? 

Do you use MacOS?

or do you use Ubuntu?

Why do you use whatever you use?

In this post, I will share the reasoning behind choosing the operating systems I use and why it matters. 

What is a distro? There are many operating systems known as [GNU/Linux](https://www.gnu.org/gnu/why-gnu-linux.html) (actually they are known erroneously as "Linux", please read [this article](https://www.gnu.org/gnu/why-gnu-linux.html) for an explanation of why we call it GNU/Linux and not Linux). The members of the family of GNU/Linux systems are called distros. 

I don't use Microsoft Windows and MacOS because they are [nonfree/proprietary software](/free-software). And the symptoms of them being nonfree software are that they are [malware](https://gnu.org/malware) and mistreat their users in many ways. 

I do not want to use any nonfree software. So, for example, Ubuntu has nonfree software in its repositories and the version of Linux, the kernel, included in Ubuntu contains firmware blobs. That is not an ideal distro I would like to run. 

Right now, I am using [PureOS](https://pureos.net/) because it does not have any nonfree software in its repositories, does not ship with any nonfree firmware, follows [Free System Distribution Guidelines (GNU FSDG)](https://www.gnu.org/distros/free-system-distribution-guidelines.html) and so [it is a GNU/FSF endorsed distro](https://www.gnu.org/distros/free-distros.en.html). Also, PureOS is ideologically [inclined towards Free Software and values user's freedom and privacy](https://puri.sm/posts/why-fsf-endorsing-pureos-matters/). 

Further, PureOS is maintained by Purism, a company which is very committed to freedom and privacy of users. Purism is developing hardware, like [mobile phones](https://puri.sm/products/librem-5/) and [laptops](https://puri.sm/products/librem-14/), which can run exclusively on fully free software. I support their work and when I say I use PureOS to someone, they might want to search about it and so it will raise awareness about Purism and their work. 

Another distribution I use and endorse is [Debian GNU/Linux](https://www.debian.org/) as it does not ship with any nonfree firmware, it does not have any nonfree packages in its main repositories. I know GNU does not endorse Debian, but I think Debian is a good freedom-respecting distro. Like PureOS, Debian is also [ideologically inclined towards free software philosophy](https://www.debian.org/intro/philosophy). Debian adheres to [Debian Social Contract](https://www.debian.org/social_contract) as well which is committed to free software. Plus Debian has an [inclusive community which is very welcoming to all](https://www.debian.org/intro/diversity). The decision-making of Debian Community is [democratic in nature](https://www.debian.org/devel/constitution). 

Further, I occassionally use other [distros endorsed by GNU](https://www.gnu.org/distros/free-distros.en.html) because they are committed to Free SOftware philosophy and contain no nonfree/proprietary software by default.   

Summarizing the above discussion, I choose distros if they match the following criteria:

1. It should respect my freedom and privacy;

2. It should be ideologically inclined to free software philosophy. 

A distro being a free software is not enough. When I use PureOS, other people will learn about Free Software and Purism. This is not the case with, say, Fedora or Ubuntu as they are not committed to free software (shipping with nonfree firmware by default is one example of this), although the operating systems are themselves Free Software. I suggest you the same if you support free software. 
