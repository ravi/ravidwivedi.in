---
title: "Are Free Software and Open-Source Software the same thing?"
date: 2021-09-25T03:40:00+05:30
type: "post"
tags: ["freesoftware", "opensource", "philosophy"]
showTableOfContents: true
---
I have seen people use the terms "Free Software" and "Open Source Software" interchangably. While it has become customary to do that, there are differences between the philosophy behind the two terms. 

Practically speaking, the terms "free software" and "open source software" refer to the same class of software. The real difference between these two terms is not the actual software but the values behind the respective movements. Free Software movement campaigns for users' freedom and that users must control the software running in their own devices. While, open-source movement does not put the issue in ethical terms like users' freedoms or rights, but more of like a practical choice.

The term "free software" existed since 1984, while the term "open source" came into existence in 1998. Let's look at why this new term was introduced even though the term "free software" was already there.

[Quoting Open Source Initiative on coining the term open source](https://opensource.org/history), 

> The “open source” label was created at a strategy session held on February 3rd, 1998 in Palo Alto, California, shortly after the announcement of the release of the Netscape source code. The strategy session grew from a realization that the attention around the Netscape announcement had created an opportunity to educate and advocate for the superiority of an open development process. The conferees believed the pragmatic, business-case grounds that had motivated Netscape to release their code illustrated a valuable way to engage with potential software users and developers, and convince them to create and improve source code by participating in an engaged community. The conferees also believed that it would be useful to have a single label that identified this approach and distinguished it from the philosophically- and politically-focused label "free software." Brainstorming for this new label eventually converged on the term "open source", originally suggested by Christine Peterson.

The difference between free software and open source lies in what they stand for and their ideology. Free Software movement campaigns for freedom of software users - the freedom to run, study, modify, share and share the modified versions of the software. Making the software proprietary is not acceptable in the spirit of Free Software. But for Open Source philosophy, open source software is just a superior model of development and not a matter of user's rights, so they can coexist with proprietary software.

Praveen has put this difference into a nice quote:

> Open Source wants to create better software, Free Software wants to create better society.

Quoting Richard Stallman from his [famous article](https://www.gnu.org/philosophy/open-source-misses-the-point.html) on the difference and the dangers that the approach open source movement has in avoiding the ethics part:

> When open source proponents talk about anything deeper than that, it is usually the idea of making a “gift” of source code to humanity. Presenting this as a special good deed, beyond what is morally required, presumes that distributing proprietary software without source code is morally legitimate.

> This approach has proved effective, in its own terms. The rhetoric of open source has convinced many businesses and individuals to use, and even develop, free software, which has extended our community—but only at the superficial, practical level. The philosophy of open source, with its purely practical values, impedes understanding of the deeper ideas of free software; it brings many people into our community, but does not teach them to defend it. That is good, as far as it goes, but it is not enough to make freedom secure. Attracting users to free software takes them just part of the way to becoming defenders of their own freedom.

> Sooner or later these users will be invited to switch back to proprietary software for some practical advantage. Countless companies seek to offer such temptation, some even offering copies gratis. Why would users decline? Only if they have learned to value the freedom free software gives them, to value freedom in and of itself rather than the technical and practical convenience of specific free software. To spread this idea, we have to talk about freedom. A certain amount of the “keep quiet” approach to business can be useful for the community, but it is dangerous if it becomes so common that the love of freedom comes to seem like an eccentricity.

Personally, I use the term "Free Software" and not "Open Source" due to my opposition towards proprietary software and agreement with the ethical values associated with the term "Free Software". Using this term helps me spread awareness about the difference and propagate the ethical values.
