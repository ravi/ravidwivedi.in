---
title: "Debconf23"
date: 2023-09-22T23:49:38+05:30
draft: false
tags: ["debian", "freesoftware", "debconf", "debconf23", "conference", "india"]
showTableOfContents: true
type: "post"
---
{{< figure src="/images/debconf23/DebConf_2023_Logo.png" loading="lazy" title="Official logo of DebConf23" width="256" >}}

## Introduction

[DebConf23](https://debconf23.debconf.org/), the 24th annual Debian Conference, was held in India in the city of Kochi, Kerala from the 3rd to the 17th of September, 2023. Ever since I got to know about it (which was more than an year ago), I was excited to attend DebConf in my home country. This was my second DebConf, as I [attended one last year in Kosovo](/posts/my-experience-attending-debconf22). I was very happy that I didn't need to apply for a visa to attend. I got full bursary to attend the event (thanks a lot to Debian for that!) which is always helpful in covering the expenses, especially if the venue is a five star hotel :)

For the conference, I submitted two talks. One was suggested by [Sahil](https://sahilister.in) on Debian packaging for beginners, while the other was suggested by Praveen who opined that a talk covering broader topics about "freedom" in self-hosting services will be better, when I started discussing about submitting a talk about [prav app project](https://prav.app). So I submitted [one on Debian packaging for beginners](https://debconf23.debconf.org/talks/81-a-beginners-guide-to-debian-packaging/) and the other on [ideas on sustainable solutions for self-hosting](https://debconf23.debconf.org/talks/83-sustainable-solutions-for-self-hosting/).

My friend Suresh - who is enthusiastic about Debian and free software - wanted to attend the DebConf as well. When the registration started, I reminded him about applying. We landed in Kochi on the 28th of August 2023 during the festival of Onam. We celebrated Onam in Kochi, had a trip to Wayanad, and returned to Kochi. On the evening of the 3rd of September, we reached the venue - Four Points Hotel by Sheraton, at Infopark Kochi, Ernakulam, Kerala, India.

{{< figure src="/images/onam-celebrations-in-kochi.jpg" loading="lazy" title="Suresh and me celebrating Onam in Kochi." width="256" >}}

## Hotel overview

The hotel had 14 floors, and featured a swimming pool and gym (these were included in our package). The hotel gave us elevator access for only our floor, along with public spaces like the reception, gym, swimming pool, and dining areas. The temperature inside the hotel was pretty cold and I had to buy a jacket to survive. Perhaps the hotel was in cahoots with winterwear companies? :)

<figure>
<img loading="lazy" 
	src="/images/debconf23/Four-Points-by-Sheraton-Hotel-Kakkanad-Kochi.jpg" 
	width="512">
<figcaption>Four Points Hotel by Sheraton was the venue of DebConf23, <a href="https://commons.wikimedia.org/wiki/File:Four_Points_by_Sheraton_Hotel,_Kakkanad,_Kochi.jpg">photo by rmb</a>
</figcaption>
</figure>

<figure>
<img loading="lazy" src="/images/debconf23/four-points-pool.jpg" width="512">
<figcaption>Photo of the pool. Photo credits: Andreas Tille.
</figcaption>
</figure>

{{< figure loading="lazy" src="/images/debconf23/outside-view-from-the-hotel-window.jpg" title="View from the hotel window." >}}

## Meals

On the first day, Suresh and I had dinner at the eatery on the third floor. At the entrance, a member of the hotel staff asked us about how many people we wanted a table for. I told her that it's just the two of us at the moment, but (as we are attending a conference) we might be joined by others. Regardless, they gave us a table for just two. Within a few minutes, we were joined by Alper from Turkey and urbec from Germany. So we shifted to a larger table...but then we were joined by even more people, so we were busy adding more chairs to our table. urbec had already been in Kerala for the past 5-6 days and was, on one hand, very happy already with the quality and taste of bananas in Kerala...and on the other, rather afraid of the spicy food :)

Two days later, the lunch and dinner were shifted to the All Spice Restaurant on the 14th floor, but the breakfast was still served at the eatery. Since the eatery (on the 3rd floor) had greater variety of food than the other venue, this move made breakfast the best meal for me and many others. Many attendees from outside India were not accustomed to the "spicy" food. It is difficult for locals to help them, because what we consider mild can be spicy for others. It is not easy to satisfy everyone at the dining table, but I think the organizing team did a very good job in the food department. (That said, it didn't matter for me after a point, and you will know why.) The pappadam were really good, and I liked the rice labelled "Kerala rice". I actually brought that exact rice and pappadam home during my last trip to Kochi and everyone at my home liked it too (thanks to [Abhijit PA](https://abhijithpa.me/)). I also wished to eat all types of payasams from Kerala and this really happened (thanks to Sruthi who designed the menu). Every meal had a different variety of payasam and it was awesome, although I didn't like some of them, mostly because they were very sweet. Meals were later shifted to the ground floor (taking away the best breakfast option which was the eatery).

<figure>
<img loading ="lazy" 
	src="/images/debconf23/hacklab.jpg"
	width="512">
<figcaption>This place served as lunch and dinner place and later as hacklab during debconf. Photo credits: Bilal</figcaption>
</figure>

## The excellent Swag Bag

The DebConf registration desk was at the second floor. We were given a very nice swag bag. They were available in multiple colors - grey, green, blue, red - and included an umbrella, a steel mug, a multiboot USB drive by [Mostly Harmless](https://mostlyharmless.io), a thermal flask, a mug by Canonical, a paper coaster, and stickers. It rained almost every day in Kochi during our stay, so handing out an umbrella to every attendee was a good idea.

<figure>
<img loading="lazy" 
	src="/images/debconf23/swag-bag.jpg"
	width="512">
<figcaption>Picture of the awesome swag bag given at DebConf23. Photo credits: Ravi Dwivedi</figcaption>
</figure>

## A gift for Nattie

During breakfast one day, Nattie (Belgium) expressed the desire to buy a coffee filter. The next time I went to the market, I bought a coffee filter for her as a gift. She seemed happy with the gift and was flattered to receive a gift from a young man :)

## Being a mentor

There were many newbies who were eager to learn and contribute to Debian. So, I mentored whoever came to me and was interested in learning. I conducted a packaging workshop in the bootcamp, but could only cover how to set up the Debian Unstable environment, and had to leave out how to package (but I covered that in my talk). Carlos (Brazil) gave a keysigning session in the bootcamp. Praveen was also mentoring in the bootcamp. I helped people understand why we sign GPG keys and how to sign them. I planned to take a workshop on it but cancelled it later.

## My talk

My Debian packaging talk was on the 10th of September, 2023. I had not prepared slides for my Debian packaging talk in advance - I thought that I could do it during the trip, but I didn't get the time...so I prepared them on the day before the talk. Since it was mostly a tutorial, the slides did not need much preparation. My thanks to Suresh, who helped me with the slides and made it possible to complete them in such a short time frame.

My talk was well-received by the audience, going by their comments. I am glad that I could give an interesting presentation.

<figure>
<img loading="lazy"
	src="/images/debconf23/ravi-talk.jpg">
<figcaption>My presentation photo. Photo credits: Valessio</figcaption>
</figure>

## Visiting a saree shop

After my talk, Suresh, Alper, and I went with [Anisa](https://anisakuci.com/) and [Kristi](https://kristiprogri.com/) - who are both from Albania, and have a never-ending fascination for Indian culture :) - to buy them sarees. We took autos to Kakkanad market and found a shop with a great variety of sarees. I was slightly familiar with the area around the hotel, as I had been there for a week. Indian women usually don't try on sarees while buying - they just select the design. But Anisa wanted to put one on and take a few photos as well. The shop staff did not have a trial saree for this purpose, so they took a saree from a mannequin. It took about an hour for the lady at the shop to help Anisa put on that saree...but you could tell that she was in heaven wearing that saree, and she bought it immediately :) Alper also bought a saree to take back to Turkey for his mother. Me and Suresh wanted to buy a kurta which would go well with the mundu we already had, but we could not find anything to our liking.

<figure>
<img loading="lazy"
     src="/images/debconf23/selfie-with-anisa-kristi.jpg">
<figcaption>Selfie with Anisa and Kristi. Photo credits: Anisa.</figcaption>
</figure>

## Cheese and Wine Party

On the 11th of September we had the Cheese and Wine Party, a tradition of every DebConf. I brought Kaju Samosa and [Nankhatai](https://en.wikipedia.org/wiki/Nankhatai) from home. Many attendees expressed their appreciation for the samosas. During the party, I was with Abhas and had a lot of fun. Abhas brought packets of paan and served them at the Cheese and Wine Party. We discussed interesting things and ate burgers. But due to the restrictive alcohol laws in the state, it was less fun compared to the previous DebConfs - you could only drink alcohol served by the hotel in public places. If you bought your own alcohol, you could only drink in private places (such as in your room, or a friend's room), but not in public places.

<figure>
<img loading="lazy"
     src="/images/debconf23/me-helping-with-cheese-and-wine-party.jpg"
     width=400px>
<figcaption>Me helping with the Cheese and Wine Party.</figcaption>
</figure>

## Party at my room

Last year, [Joenio](http://joenio.me/) (Brazilian) brought pastis from France which I liked. He brought the same alocholic drink this year too. So I invited him to my room after the Cheese and Wine party to have pastis. My idea was to have them with my roommate Suresh and Joenio. But then we permitted Joenio to bring as many people as he wanted...and he ended up bringing some ten people. Suddenly, the room was crowded. I was having good time at the party, serving them the snacks given to me by Abhas. The news of an alcohol party at my room spread like wildfire. Soon there were so many people that the AC became ineffective and I found myself sweating.

I left the room and roamed around in the hotel for some fresh air. I came back after about 1.5 hours - for most part, I was sitting at the ground floor with TK Saurabh. And then I met Abraham near the gym (which was my last meeting with him). I came back to my room at around 2:30 AM. Nobody seemed to have realized that I was gone. They were thanking me for hosting such a good party. A lot of people left at that point and the remaining people were playing songs and dancing (everyone was dancing all along!). I had no energy left to dance and to join them. They left around 03:00 AM. But I am glad that people enjoyed partying in my room.

<figure>
<img loading="lazy" src="/images/debconf23/my-room-party.jpg">
<figcaption>This picture was taken when there were few people in my room for the party.</figcaption>
</figure>

## Sadhya Thali

On the 12th of September, we had a sadhya thali for lunch. It is a vegetarian thali served on a banana leaf on the eve of Thiruvonam. It wasn't Thiruvonam on this day, but we got a special and filling lunch. The rasam and payasam were especially yummy.

<figure>
<img loading="lazy" src="/images/debconf23/sadhya-thali.jpg">
<figcaption>Sadhya Thali: A vegetarian meal served on banana leaf. Payasam and rasam were especially yummy! Photo credits: Ravi Dwivedi.</figcaption>
</figure>

<figure>
<img loading="lazy"
     src="/images/debconf23/sadhya-being-served.jpg">
<figcaption>Sadhya thali being served at debconf23. Photo credits: Bilal</figcaption>
</figure>

## Day trip

On the 13th of September, we had a [daytrip](https://wiki.debian.org/DebConf/23/DayTrip). I chose the daytrip houseboat in Allepey. Suresh chose the same, and we registered for it as soon as it was open. This was the most sought-after daytrip by the DebConf attendees - around 80 people registered for it.

Our bus was set to leave at 9 AM on the 13th of September. Me and Suresh woke up at 8:40 and hurried to get to the bus in time. It took two hours to reach the venue where we get the houseboat.

The houseboat experience was good. The trip featured some good scenery. I got to experience the renowned Kerala backwaters. We were served food on the boat. We also stopped at a place and had coconut water. By evening, we came back to the place where we had boarded the boat.

<figure>
<img loading="lazy" src="/images/debconf23/daytrip-group-photo.jpg">
<figcaption>Group photo of our daytrip. Photo credits: Radhika Jhalani</figcaption>
</figure>

## A good friend lost

When we came back from the daytrip, we received news that Abhraham Raji was involved in a fatal accident during a kayaking trip.

Abraham Raji was a very good friend of mine. In my Albania-Kosovo-Dubai trip last year, he was my roommate at our Tirana apartment. I [roamed around in Dubai](/posts/layover-in-dubai) with him, and we had many discussions during DebConf22 Kosovo. He was the one who took the photo of me on my homepage. I also met him in MiniDebConf22 Palakkad and MiniDebConf23 Tamil Nadu, and went to his flat in Kochi this year in June.

We had many projects in common. He was a Free Software activist and was the designer of the DebConf23 logo, in addition to those for other Debian events in India.

<figure>
<img loading="lazy" src="/images/debconf23/a-pic-with-abraham.jpg">
<figcaption>A selfie in memory of Abraham.</figcaption>
</figure>

We were all fairly shocked by the news. I was devastated. Food lost its taste, and it became difficult to sleep. That night, Anisa and Kristi cheered me up and gave me company. Thanks a lot to them.

The next day, Joenio also tried to console me. I thank him for doing a great job. I thank everyone who helped me in coping with the difficult situation.

On the next day (the 14th of September), the Debian project leader Jonathan Carter addressed and announced the news officially. THe Debian project also [mentioned](https://www.debian.org/News/2023/20230914) it on their website.

Abraham was supposed to give a talk, but following the incident, all talks were cancelled for the day. The conference dinner was also cancelled.

As I write, 9 days have passed since his death, but even now I cannot come to terms with it.

## Visiting Abraham's house

On the 15th of September, the conference ran two buses from the hotel to Abraham's house in Kottayam (2 hours ride). I hopped in the first bus and my mood was not very good. Evangelos (Germany) was sitting opposite me, and he began conversing with me. The distraction helped and I was back to normal for a while. Thanks to Evangelos as he supported me a lot on that trip. He was also very impressed by my use of the StreetComplete app which I was using to edit OpenStreetMap.

In two hours, we reached Abraham's house. I couldn't control myself and burst into tears. I went to see the body. I met his family (mother, father and sister), but I had nothing to say and I felt helpless. Owing to the loss of sleep and appetite over the past few days, I had no energy, and didn't think it was good idea for me to stay there. I went back by taking the bus after one hour and had lunch at the hotel. I withdrew my talk scheduled for the 16th of September.

## A Japanese gift

I got a nice Japanese gift from Niibe Yutaka (Japan) - a folder to keep papers which had ancient Japanese manga characters. He said he felt guilty as he swapped his talk with me and so it got rescheduled from 12th September to 16 September which I withdrew later.

<figure>
<img loading="lazy" src="/images/debconf23/photo-with-Niibe-Yutaka.jpg">
<figcaption>Thanks to Niibe Yutaka (the person towards your right hand) from Japan (FSIJ), who gave me a wonderful Japanese gift during debconf23: A folder to keep pages with ancient Japanese manga characters printed on it. I realized I immediately needed that :)</figcaption>
</figure>

<figure>
<img loading="lazy" src="/images/debconf23/japanese-gift.jpg">
<figcaption>This is the Japanese gift I received.</figcaption>
</figure>

## Group photo

On the 16th of September, we had a group photo. I am glad that this year I was more clear in this picture than in DebConf22.

<figure>
<img loading="lazy"
     src="https://salsa.debian.org/debconf-team/public/share/debconf23/-/raw/main/photos/aigarius/group/debconf23_group.jpg"
     width=700px>
<figcaption><a href="https://salsa.debian.org/debconf-team/public/share/debconf23/-/blob/main/photos/aigarius/group/debconf23_group.jpg">Click to enlarge</a></figcaption>
</figure>

## Volunteer work and talks attended

I attended the training session for the video team and worked as a camera operator. The [Bits from DPL](https://debconf23.debconf.org/talks/60-bits-from-the-dpl/) was nice. I enjoyed Abhas' presentation on [home automation](https://debconf23.debconf.org/talks/94-home-automation-using-free-software/). He basically demonstrated how he liberated Internet-enabled home devices. I also liked Kristi's presentation on [ways to engage with the GNOME community](https://debconf23.debconf.org/talks/35-gnome-community-and-ways-to-engage/).

<figure>
<img loading="lazy"
     src="/images/debconf23/bits-from-the-dpl.jpg">
<figcaption>Bits from the DPL. Photo credits: Bilal</figcaption>
</figure>

<figure>
<img loading="lazy" src="/images/debconf23/kristi-talk.jpg">
<figcaption>Kristi on GNOME community. Photo credits: Ravi Dwivedi.</figcaption>
</figure>

<figure>
<img loading="lazy" src="/images/debconf23/abhas-talk.jpg" width=700px>
<figcaption>Abhas' talk on home automation. Photo credits: Ravi Dwivedi.</figcaption>
</figure>

I also attended lightning talks on the last day. Badri, Wouter, and I gave a demo on how to register on the [Prav app](https://prav.app). Prav got a fair share of advertising during the last few days.

<figure>
<img loading="lazy" src="https://salsa.debian.org/debconf-team/public/share/debconf23/-/raw/main/photos/ravi/prav/2023-09-17-18-57-32-508.jpg">
<figcaption>I was roaming around with a QR code on my T-shirt for downloading Prav.</figcaption>
</figure>

## Departure day

The 18th of September was the day of departure. Badri slept in my room and left early morning (06:30 AM). I dropped him off at the hotel gate. The breakfast was at the eatery (3rd floor) again.

I had an 8 PM flight from Kochi to Delhi, for which I took a cab with Rhonda (Austria), Michael (Nigeria) and Yash (India). We were joined by other DebConf23 attendees at the Kochi airport, where we took another selfie.

<figure>
<img loading="lazy" src="/images/debconf23/photo-at-kochi-airport.jpg" width="512">
<figcaption>Ruchika (taking the selfie) and from left to right: Yash, <a href="http://mdcc.cx">Joost (Netherlands)</a>, me, Rhonda</figcaption>
</figure>

Joost and I were on the same flight, and we sat next to each other. He then took a connecting flight from Delhi to Netherlands, while I went with Yash to the New Delhi Railway Station, where we took our respective trains. I reached home on the morning of the 19th of September, 2023.

<figure>
<img loading="lazy" src="/images/debconf23/selfie-with-joost.jpg">
<figcaption>Joost and me going to Delhi. Photo credits: Ravi.</figcaption>
</figure>

## Big thanks to the organizers

DebConf23 was hard to organize - strict alcohol laws, weird hotel rules, death of a close friend (almost a family member), and a scary notice by the immigration bureau. The people from the team are my close friends and I am proud of them for organizing such a good event.

None of this would have been possible without the organizers who put more than a year-long voluntary effort to produce this. In the meanwhile, many of them had organized local events in the time leading up to DebConf. Kudos to them.

The organizers also tried their best to get clearance for countries not approved by the ministry. I am also sad that people from China, Kosovo, and Iran could not join. In particular, I feel bad for people from Kosovo who wanted to attend but could not (as India does not consider their passport to be a valid travel document), considering how we Indians were so well-received in their country last year.

## Note about myself

I am writing this on the 22nd of September, 2023. It took me three days to put up this post - this was one of the tragic and hard posts for me to write. I have literally forced myself to write this. I have still not recovered from the loss of my friend. Thanks a lot to all those who helped me.

PS: <b>Credits to <a href="https://contrapunctus.codeberg.page/">contrapunctus</a> for making <a href="https://codeberg.org/ravidwivedi/ravidwivedi.in/pulls/1">grammar, phrasing, and capitalization changes</a></b>.

