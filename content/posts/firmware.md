---
title: "Ethical issues regarding firmware"
date: 2021-09-06
draft: false
type: "post"
showTableOfContents: true
---

### Index

- [What is firmware](#what-is-firmware)

- [Problems with proprietary firmware](#problems-with-proprietary-firmware)

- [Importance of Free Firmware](#importance-of-free-firmware)

### What is firmware

Firmware is a software which provides a way for hardware to interact with the operating system. It runs on a secondary processor and not on the CPU. For example the WiFi chipset will run this code directly instead of the main CPU. Firmware files aren’t used by the kernel, they’re loaded by the kernel onto other pieces of hardware. 

[Quoting Debian wiki](https://wiki.debian.org/Firmware) on what is Firmware:

> Firmware refers to embedded software which controls electronic devices. Well-defined boundaries between firmware and software do not exist, as both terms cover some of the same code. Typically, the term firmware deals with low-level operations in a device, without which the device would be completely non-functional.

For example, remote control of a television uses firmware to convert your button presses into infrared signals so that the television can understand. For more examples of firmware, please [check this wiki page](https://en.wikipedia.org/wiki/Firmware#Examples).

Let's suppose you are able to boot and install a [free software](/posts/free-sw) operating system, like any GNU/Linux based Operating System and you did not install any nonfree software. You might be tempted to say that you are running only free software but you might still be running proprietary firmware. For example, Ubuntu comes pre-installed with nonfree firmware. Depending upon your laptop, you might need to install nonfree firmware in addition to the operating system for some hardware to work properly.

### Problems with proprietary firmware

The problem with propietary firmware is the same as any [propietary software](/posts/free-sw)-- users do not control it and we cannot trust it. It does not give users the freedom and therefore users do not have a defence against any of the bugs or intentional malfunctionalities being introduced in the system. 

[Quoting Mark Shuttleworth](https://web.archive.org/web/20150315054919/http://markshuttleworth.com/archives/1332), the founder of Canonical which develops and maintains Ubuntu,

> If you read the catalogue of spy tools and digital weaponry provided to us by Edward Snowden, you’ll see that firmware on your device is the NSA’s best friend.

Following are some known examples of insecurity issues due to bugs or adding intentional malfunctionalities at firmware level: 

- A researcher broke into Apple's firmware and caused the [battery to overcharge](https://arstechnica.com/gadgets/2011/07/how-charlie-miller-discovered-the-apple-battery-hackhow-a-security-researcher-discovered-the-apple-battery-hack/)

- NSA adds [spyware into firmware of many devices](https://www.wired.com/2015/02/nsa-firmware-hacking/).

- Wikileaks [revealed that several CIA projects infect Apple Mac firmware](https://wikileaks.org/vault7/#Dark%20Matter) (meaning the infection persists even if the operating system is re-installed). Basically, EFI/UEFI is essentially a backdoor for taking control of a computer without user's knowledge. Further, the attacks described in the leaks are very widespread. 

- A vulnerability in the EFI/UEFI firmware is used by CIA [to write to NVRAM or persistent storage of the computer](https://wikileaks.org/ciav7p1/cms/page_31227915.html) without the users knowing about it. It cannot be detected easily by the users and it can survive a complete OS reinstall. 

- CIA documents leaked by the Wikileaks note that the UEFI level exploits can compromise the whole system. The ExitBootServices Hooking page notes, "[At this point, you can do whatever you want.](https://wikileaks.org/ciav7p1/cms/page_36896783.html)"

Keep in mind that if a backdoor is installed at the firmware level, it can gain full access to your computer. Such a control of the system means that the applications running on your machine are also fully compromised. 

### Importance of free firmware 

In the previous section, we explored that the issue of firmware being propietary and hence, not controlled by the user, is not a light issue at all. It is a very serious issue and the well-known exploits in these firmware can make you a prey to NSA or any third-party spying without you even knowing. 

The firmware should be free software as any software so that the users have freedom to detect and fix bugs, remove any backdoors put into the firmware, and other problems that can arise with proprietary firmware. For this, the manufacturers need to publish key technical specifications sufficient to write free firmware for their hardware. Even if you are running proprietary firmware and ditch Windows or MacOS or any proprietary operating system to switch to a free software operating system, like GNU/Linux, I welcome your move as one more step towards freedom.

A very important example of a firmware is [BIOS](https://en.wikipedia.org/wiki/BIOS), which starts your computer when you power it on. [Libreboot](http://www.libreboot.org/) and [Coreboot](https://www.coreboot.org/) are free software BIOS.  

[Liberated Computer](/posts/liberated-computer) (the laptop I use) runs on Coreboot and the only nonfree software it has is the Intel ME blob which is disabled.That means it runs purely free software. 

The following devices have only free firmware installed at the time of shipping: 

- [Devices which can run Replicant OS](https://redmine.replicant.us/projects/replicant/wiki#Supported-devices) (Note: You might have to use external hardware/dongles for WiFi, Bluetooth to work)

- [Devices that have Libreboot support](https://libreboot.org/suppliers.html) 

- [Devices which are RYF certified by FSF](https://ryf.fsf.org/categories/laptops)

Unfortunately, proprietary firmware is not even considered a problem by many people who care about software freedom. Perhaps there is a lack of awareness of the issue. If that is the case, then hopefully I have now made you aware of the issues with proprietary firmware. 

(Credits: Thanks to [Pirate Praveen](https://social.masto.host/@praveen) for proofreading the article and correcting some factual errors.)

### For further reading:

- [Debian wiki](https://wiki.debian.org/Firmware).
