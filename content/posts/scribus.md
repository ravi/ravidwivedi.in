---
title: "Scribus: When Freedom-Respecting Software Saved The Day" 
date: 2021-11-24
draft: false
type: "post"
showTableOfContents: true
---
Janayugom is a newspaper in Kerala, India, which publishes news in Malayalam language. Earlier they were using Adobe Pagemaker for publishing which supported only ASCII encoding. They considered using a better software for publishing in Malayalam. Many suggested them to try Adobe InDesign, but they realized that they need to pay a hefty subscription fee, which they could not afford. They came across community members of FSCI (Free Software Community of India), who suggested them to use Scribus for their publishing work. They don't have to pay for any subscription fee to use it. Plus it supports Malayalam.

The developers of Scribus only added support for Latin languages, like English, Spanish, etc. Oman government funded adding support for Non-Latin languages in Scribus with Complex Text Layout feature, because they wanted support for Arabic language. This made is possible to add Malayalam support to Scribus.

This was done independent of the developers of Scribus. It is because Scribus is Free Software (free as in freedom, not price), which means anyone can adapt Scribus according to their needs. Such a thing is not possible with those Adobe software as they are proprietary and only developers can make changes. 

What will you do if the developer of a proprietary software does not care about the feature you want to add? Maybe the developers don't care about supporting your language. In the case of Free Software, such changes are possible, independent of developers' wish. Further, the funded project by the Oman government helped people in India to publish in their language. It is because users get freedom to share the modified versions of Free Software. And thus, any addition of feature helps the whole society.
