---
title: "Contribute to Wikimedia Commons"
date: 2021-11-17
draft: false
type: "post"
showTableOfContents: true
---
If you have an image or a photo that you have taken which is [useful for informative purposes](https://commons.wikimedia.org/wiki/Commons:Project_scope#Must_be_realistically_useful_for_an_educational_purpose), you can upload them to [Wikimedia Commons](https://commons.wikimedia.org).

All users of files found on Wikimedia Commons must be given the [Four Freedoms](https://freedomdefined.org/Definition):

- The freedom to use the media.
- The freedom to study the media and use information gained from it.
- The freedom to make and distribute copies of the media.
- The freedom to make changes to the media and distribute derived versions.

I publish my images under a copyleft license, which means if you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original, thus protecting every user's (whoever uses that image) freedom. One such license is [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). 

You can upload your images to Wikimedia Commons as well. Whenever you have some photo you shot and want to contribute it to public so that others can use it in their work, upload it to Wikimedia Commons. 

A federated and decentralized version of image sharing Wikimedia Commons would be good. Currently it is centralized and all the images are on one server. We can store these images at many places to avoid single point of failure.

Thanks to [sahilister](https://sahilister.in), who suggested me to upload there.  
