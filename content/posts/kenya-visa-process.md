---
title: "Kenya Visa Process"
date: 2024-07-14T16:24:02+05:30
draft: false
type: "post"
tags: ["travel", "visa", "kenya", "africa"]
showTableOfContents: true
---
Prior to arrival in Kenya, you need to apply for an Electronic Travel Authorization (eTA) on [their website](https://www.etakenya.go.ke/en) by uploading all the required documents. This system is in place since Jan 2024 after the country abolished the visa system. The required documents will depend on the purpose of your visit, which in my case, was to attend a conference.

Here is the list of documents I submitted for my eTA:

- Scanned copy of my passport

- Photograph with white background

- Flight tickets (reservation)

- Hotel bookings (reservation)

- Invitation letter from the conference

- Yellow Fever vaccination certificate (optional)

- Job contract (optional)

"Reservation" means I didn't book the flights and hotels, but rather reserved them. Additionally, "optional" means that those documents were not mandatory to submit, but I submitted them in the "Other Documents" section in order to support my application. After submitting the eTA, I had to make a payment of around 35 US Dollars (approximately 3000 Indian Rupees). 

It took 40 hours for me to receive an email from Kenya stating that my eTA has been approved, along with an attached PDF, making this one of my smoothest experiences of obtaining travel documents to travel to a country :). An eTA is technically not a visa, but I put the word "visa" in the title due to familiarity with the term. 
