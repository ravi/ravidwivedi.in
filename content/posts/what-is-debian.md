---
title: "What is Debian"
date: 2022-06-26T13:55:52+05:30
tags: ["debian", "freesoftware"]
draft: false
showTableOfContents: true
type: "post" 
---
### Debian as an operating system

<figure> 
<img src="/images/homeworld_desktop.png"; width="700">
<figcaption>Debian desktop.
<br>
Credits: <a href="http://www.juliettetaka.com/">Juliette Taka</a>. License: CC-BY-SA-4.0. Source: <a href="https://wiki.debian.org/DebianArt/Themes/Homeworld">Debian wiki</a>.</figcaption>
</figure>

[Debian](https://debian.org) GNU/Linux is a [Free Software](/free-software) (software which respects user's freedom to run, study, modify, share and share the modified versions) operating system. Debian is committed to Free Software philosophy, as software freedom is deeply embedded in its [social contract](https://www.debian.org/social_contract). Debian's official image ships with only free software including firmware, and the official repositories also contain only Free Software. If users need nonfree drivers, then separate images with nonfree firmware are also available on the Debian website.

<figure> 
<img src="/images/homeworld.jpeg"; width="700">
<figcaption>Debian's tagline is 'The Universal operating system.'
<br>
Credits: <a href="http://www.juliettetaka.com/">Juliette Taka</a>. License: CC-BY-SA-4.0. Source: <a href="https://wiki.debian.org/DebianArt/Themes/Homeworld">Debian wiki</a>.</figcaption>
</figure>

Debian operating system has three branches: [stable](https://wiki.debian.org/DebianStable), [testing](https://www.debian.org/doc/manuals/debian-faq/ftparchives#testing) and [unstable](https://www.debian.org/doc/manuals/debian-faq/ftparchives.en.html#unstable). Debian explains that [the main goal of the project is to develop the stable branch](https://wiki.debian.org/DebianStability). Other branches are only a means for that end. Of course, many users might still find it suitable to install testing or unstable, keeping in mind that these might break.

Debian is known for its stability and its universality. The stability of Debian stable branch comes from the fact that it is extensively tested before the release as the testing branch. The three branch system allows the user to choose the desired stability. The stable branch gets only security updates and mission critical bug fixes. So, the packages in stable branch might not be the latest version, but they are tested thoroughly before putting there. Debian Stable users can download recent releases of some software from the [Debian backports repository](https://backports.debian.org/). Stable branch is a recommended one for a company's production machine or school labs and personal computers too. 

Debian can run on all types of hardware and supports [9 CPU architectures](https://www.debian.org/ports/) and some others unofficially as well. It can run on your personal computer, a school computer, old hardware, servers, Raspberry Pi and similar devices, IoT devices, etc. Whatever piece of hardware you have, you can put Debian on it, and that is why Debian's tagline is 'The Universal Operating System'. This is [far more than](https://www.debian.org/doc/manuals/debian-faq/basic-defs.en.html#difference) is available for any other GNU/Linux distribution.

Many popular GNU/linux distributions are based on Debian. Examples are: [Ubuntu](https://ubuntu.com), [PureOS](https://puri.sm/posts/what-is-pureos-and-how-is-it-built/), [Tails OS](https://tails.boum.org/) etc.

### People behind Debian

Debian's biggest feature is the community behind it and how that community makes decisions. The guiding principles of Debian community are [the Debian Social Contract](https://www.debian.org/social_contract) and the [Debian Free Software guidelines](https://www.debian.org/social_contract#guidelines).

<figure>
<img src="https://www.debian.org/Pics/Debconf19_group_photo.jpg"; alt="DebConf19 group photo">
<figcaption><a href="https://debconf19.debconf.org/">DebConf19</a> group photo.
<br>
Source: <a href="https://www.debian.org/Pics/"> Debian Pics</a>.
<br>
License:<a href="https://www.debian.org/legal/licenses/mit">MIT License (Expat)</a>. <a href="https://www.debian.org/license">Debian license page</a>.
</figcaption>
</figure>

In short, the Debian Social contract promises the following to its users: 1) 100% Free Software; 2) Debian project will give back to Free Software community; 3) Problems won't be hidden; 4) Users are priority; 5) providing support for nonfree packages to users, if required.

The track record of 29 years of the project says that these promises have never been broken. It is an independent project made by volunteers [all around the globe](https://www.debian.org/devel/developers.loc). It has a democratic decision-making structure as directed by its [constitution](https://www.debian.org/devel/constitution). 

Companies or individuals sponsoring Debian project do not get any say in the decisions of the Debian project just because they gave money to the project.

Debian project's democratic decision making along with software freedom of the operating system [makes it one of the most secure operating systems](https://lists.debian.org/debian-security/2022/04/msg00047.html), since Debian does not have a single point of failure.

This works as follows:

Every Debian Developer's gpg keys are [signed](http://en.wikipedia.org/wiki/Digital_signature) by at least two other existing Debian developers. It is a decentralized trust model and forms a [web of trust](https://www.gnupg.org/gph/en/manual.html#AEN385). Every package uploaded to Debian is compiled from its source code and signed by the private gpg keys of a Debian Developer. Only the owner of the keys can have their private keys. A maintainer of a Debian package abusing their position to upload malicious code intentionally will harm their own reputation in the society as every upload is public and it will stay there for lifetime. Compare this to a centralized corporation where they can upload malicious code in the packages but this commit is not public.

How can we make sure that the binaries match the source code? Debian is also working on [reproducible builds](https://wiki.debian.org/ReproducibleBuilds), so that users can build the package on their own and verify that the binary actually matches the source code. Purism explained the concept very well in their [blog post](https://puri.sm/posts/freedom-from-coercion/).

Debian has a separate [LTS team](https://wiki.debian.org/LTS) which provides support to Debian stable releases for at least 5 years. This can be helpful for companies and organizations who want support in their Debian usage.

In conclusion, Debian is a freedom-respecting operating system maintained and developed by its users around the globe. It is not controlled by a single entity and this makes it hard to compromise, making it one of the most secure operating systems on the earth. 

### My experience

I started using Debian in August 2021 and I have tried all the branches of Debian: stable, testing and unstable. I was not using only Debian in this time frame and have switched to PureOS for around 6 months. Personally, I faced no problems in using Debian. I found that even testing branch is stable enough to be my daily driver. Currently, I am running the stable branch of Debian Bullseye, along with adding backports to my sources file. The Desktop environment that has suited me the most as of now is KDE Plasma. I love it and whenever I switch to any other Desktop Environment, I do not feel at home. 

<figure>
<img src="/images/debian-bullseye-kde-plasma.png"; width="700">
<figcaption>Debian with KDE Plasma desktop. Released under CC-BY-SA-4.0</figcaption>
</figure>

### Read further

Want to install Debian in your computer? or curious about how to contribute to Debian? Please check:

- [Get Debian](https://www.debian.org/distrib/) for your device.

- [Contribute to Debian](https://www.debian.org/devel/join/).

- An [Indian shop](https://libretech.shop/product/lc230/) sells hardware that can run fully free version of Debian. 

- Buy a computer with [Debian pre-installed](https://www.debian.org/distrib/pre-installed).

- Clevo is [shipping laptops](https://laptopwithlinux.com/buy-debian-laptop-notebooks-with-debian-linux-preinstalled/) with Debian out-of-the-box. 

- [Librem hardware](https://puri.sm/products/)(like laptops and mobile phones) [can run Debian](https://puri.sm/faq/can-i-install-a-different-os-on-my-librem-laptop/).

- If you have any technical questions, please feel free to ask any of the [Debian user support channels](https://www.debian.org/support) or [Free Software Community of India](https://fsci.in/tech-support/).
