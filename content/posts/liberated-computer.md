---
title: "Liberated Computer Review"
date: 2021-09-05
draft: false
type: "post"
showTableOfContents: true
---
Update: Added some pictures on 29-April-2022.

### What is Liberated Computer

[Liberated Computer](https://liberated.computer) is a computer sold by [LibreTech Shop](https://libretech.shop) based in Bangalore, India, which can run exclusively on [free software](/posts/free-sw/) (which means it respects user's freedom and not that it is free-of-cost) and does not ship with any proprietary software installed. Liberated Computers are basically old and refurbished Lenovo's Thinkpad laptops, modified in several ways so that they can run purely free software (check the section "How is an LC230 assembled?" in [LC 230 docs](https://docs.libretech.shop/lc230/)). In addition, it respects user's hardware freedom as well-- you can do whatever you wish to do with the hardware, with no locks or constraints imposed by the manufacturer. 

The computer is liberated by replacing the BIOS which won't let a chip run which is not in its allow list. Further, the [Intel ME backdoor](https://www.fsf.org/blogs/sysadmin/the-management-engine-an-attack-on-computer-users-freedom) has been disabled. 

It has Coreboot whose code is 100% free software. The x230 BIOS contains the Intel management engine - which is then neutered using me_cleaner. That is a blob (A blob means a device driver whose source code is not published, only the binary is published; blobs are nonfree software) but that blob is inactive during the course of normal working of the operating system. There is no other blob in Liberated Computer. I will explain why free firmware matters in the next post. This makes Liberated Computer run on 100% free software. 

Here is a screenshot of a tweet by Leah Rowe, the founder of Libreboot project, on Lenovo x230(Remember that Liberated Computer is just refurbished Lenovo's x230) and Coreboot. 

![Tweet by Leah Rowe](/images/leah-rowe.png "Tweet by Leah Rowe on x230 capable of running 100% free software")

### Why I bought Liberated Computer

I bought a Liberated Computer and the reason is simple: the attitude that freedom comes first and features are secondary. Free Software provides users the freedom to run, study, modify, improve, share the software. If the software lacks any of these freedoms, it is called nonfree/proprietary software. 

I think free software is necessary for:

- privacy and security of users, though it might not be sufficient;

- learning and doing things ourselves; 

- having a defence against the mistreatment by [proprietary software](https://gnu.org/malware).

There are many other reasons to use free software. 

I also think that the freedoms provided by free software are every user's rights. I am not of the opinion that we use any software that works, but the software should respect user's freedom. Naturally, I would like to run only free software and eliminate all the nonfree software from my life. 

### My Purchase

I used to have Apple's Macbook which is a very locked system and [you don't own it even after purchasing it](https://sneak.berlin/20201112/your-computer-isnt-yours/). After I got convinced that software freedom is important, I tried to use only free software and avoid proprietary software as much as possible. However, a lot of free software for my use was not available for Macbook. And I doubt if Macbook can run without any proprietary software. Therefore, I was looking for a computer which can run exclusively on free software, without requiring any proprietary software. I knew that Librem laptops can run purely free software and they satisfied my requirement. I didn't know about any Indian vendors selling such a laptop. One day, I asked in the FSCI matrix room whether there are Indian shops selling such a laptop. I came to know about [Libretech Shop](http://libretech.shop/) based in Bangalore which sells free software powered laptops and calls them '[Liberated Computer](https://liberated.computer/)'. A few months later, I decided to buy a [LC 230](https://libretech.shop/product/lc230/).

I ordered LC 230 using the [Mostly Harmless website](https://mostlyharmless.io), with 16 GB RAM, 480 GB SSD, and a new 6-cell battery. 

| Item Description                     | Amount (INR)  |
|--------------------------------------|----------------|
| Base Price                           | ₹ 27,000.00    |
| RAM (16 GB)                         | ₹ 3,500.00     |
| SSD (480 GB)                        | ₹ 2,500.00     |
| New Battery (6-cell Battery)        | ₹ 3,500.00     |
| **Subtotal**                        | **₹ 36,500.00**|
| Shipping (Flat Rate)                | ₹ 1,200.00     |
| GST                                  | ₹ 6,786.00     |
| **Total**                           | **₹ 44,486.00**|


I placed my order on a Saturday. Abhas shipped it to me, three days later, on Tuesday via DTDC priority shipping and I received the laptop on Thursday. 

### My experience with LC 230

The laptop had KDE Neon pre-installed but Abhas (the owner of the shop where I purchased Liberated Computer) suggested me to re-install any OS of my choice on my own. I re-installed KDE Neon using a bootable USB. I explored KDE Neon and liked it. I found it user-friendly and got comfortable in a few hours.

Then I booted PureOS, Ubuntu, Kubuntu, Manjaro KDE, Solus Budgie etc. Finally, I settled on Debian 11 KDE and I am using it as my main OS right now. 

The new battery lasts around 5 hours from full charged state to zero.

The low speaker volume is somewhat offputting. Instead of a backlit keyboard, it has a [Thinklight](https://en.wikipedia.org/wiki/ThinkLight) which flashes light on the keyboard to work in the dark. 

It has hardware kill switches for microphone, audio and wireless connections. It has a webcam too. 

### Switched to GNU/Linux

Finally, I switched to GNU/Linux. The GNU/Linux operating systems are free software and are fully under user's control and this is exactly my reason for the switch. Nonfree software [macOS](https://sneak.berlin/20201112/your-computer-isnt-yours/) and [Windows](https://privacytools.io/operating-systems/#win10) are full of trackers. Debian, on the other hand, does not have any trackers or spywares. It respects my freedom and privacy. 

Happy Hacking!

### Pictures

Following are some pictures of Liberated Computer taken by me. All of them are released under CC-BY-SA license.

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Official_iso_of_Debian_Bullseye_with_Mate_desktop.jpg"><img width="512" alt="Official iso of Debian Bullseye with Mate desktop" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Official_iso_of_Debian_Bullseye_with_Mate_desktop.jpg/512px-Official_iso_of_Debian_Bullseye_with_Mate_desktop.jpg"></a>
<figcaption>Liberated Computer running official ISO(which has free firmware) of <a href="https://debian.org">Debian GNU/Linux</a> with Mate desktop.</figcaption>
</figure>
<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Liberated_Computer_running_PureOS_3.jpg"><img width="512" alt="Liberated Computer running PureOS 3" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Liberated_Computer_running_PureOS_3.jpg/512px-Liberated_Computer_running_PureOS_3.jpg"></a>
<figcaption> Liberated Computer running <a href="https://pureos.net">PureOS</a>.</figcaption>
</figure>
<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Liberated_Computer.jpg"><img width="512" alt="Liberated Computer" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Liberated_Computer.jpg/512px-Liberated_Computer.jpg"></a>
<figcaption> Liberated Computer running <a href="https://pureos.net">PureOS</a>.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Liberated_Computer_running_PureOS_2.jpg"><img width="512" alt="Liberated Computer running PureOS 2" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Liberated_Computer_running_PureOS_2.jpg/512px-Liberated_Computer_running_PureOS_2.jpg"></a>
<figcaption> Liberated Computer running <a href="https://pureos.net">PureOS</a>.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Liberated_Computer_running_Coreboot.jpg"><img width="512" alt="Liberated Computer running Coreboot" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Liberated_Computer_running_Coreboot.jpg/512px-Liberated_Computer_running_Coreboot.jpg"></a>
<figcaption> Liberated Computer running Coreboot.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Liberated_Computer_keyboard_and_touchpad.jpg"><img width="512" alt="Liberated Computer keyboard and touchpad" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Liberated_Computer_keyboard_and_touchpad.jpg/512px-Liberated_Computer_keyboard_and_touchpad.jpg"></a>
<figcaption> Liberated Computer keypad and touchpad.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Tails_OS_running_in_Liberated_Computer.jpg"><img width="512" alt="Tails OS running in Liberated Computer" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Tails_OS_running_in_Liberated_Computer.jpg/512px-Tails_OS_running_in_Liberated_Computer.jpg"></a>
<figcaption>Tails OS running in Liberated Computer.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Fedora_35_live_USB_running_in_Liberated_Computer.jpg"><img width="512" alt="Fedora 35 live USB running in Liberated Computer" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Fedora_35_live_USB_running_in_Liberated_Computer.jpg/512px-Fedora_35_live_USB_running_in_Liberated_Computer.jpg"></a>
<figcaption>Fedora 35 live USB running in Liberated Computer.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Ubuntu_live_USB_running_in_Liberated_Computer.jpg"><img width="512" alt="Ubuntu live USB running in Liberated Computer" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Ubuntu_live_USB_running_in_Liberated_Computer.jpg/512px-Ubuntu_live_USB_running_in_Liberated_Computer.jpg"></a>
<figcaption>Ubuntu 20.04.3 LTS live USB running in Liberated Computer.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Ubuntu_running_in_Liberated_Computer.jpg"><img width="512" alt="Ubuntu running in Liberated Computer" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Ubuntu_running_in_Liberated_Computer.jpg/512px-Ubuntu_running_in_Liberated_Computer.jpg"></a>
<figcaption>Ubuntu Desktop in Liberated Computer.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Manjaro_KDE_21.2.1_running_on_Liberated_Computer.jpg"><img width="512" alt="Manjaro KDE 21.2.1 running on Liberated Computer" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Manjaro_KDE_21.2.1_running_on_Liberated_Computer.jpg/512px-Manjaro_KDE_21.2.1_running_on_Liberated_Computer.jpg"></a>
<figcaption>Manjaro KDE 21.2.1 running on Liberated Computer.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Trisquel_10_running_in_Liberated_Computer.jpg"><img width="512" alt="Trisquel 10 running in Liberated Computer" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Trisquel_10_running_in_Liberated_Computer.jpg/512px-Trisquel_10_running_in_Liberated_Computer.jpg"></a>
<figcaption>Trisquel 10 desktop in Liberated Computer.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Manjaro_running_in_Liberated_Computer.jpg"><img width="512" alt="Manjaro running in Liberated Computer" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Manjaro_running_in_Liberated_Computer.jpg/512px-Manjaro_running_in_Liberated_Computer.jpg"></a>
<figcaption>Manjaro running in Liberated Computer.</figcaption>
</figure>

<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Trisquel_10.0_running_in_Lenovo_Thinkpad_x230.jpg"><img width="512" alt="Trisquel 10.0 running in Lenovo Thinkpad x230" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Trisquel_10.0_running_in_Lenovo_Thinkpad_x230.jpg/512px-Trisquel_10.0_running_in_Lenovo_Thinkpad_x230.jpg"></a>
<figcaption>Trisquel 10.0 running in Liberated Computer.</figcaption>
</figure>
