---
title: "Hindi typing in Debian-based distros and making slides in Devanagari"
date: 2022-02-07
tags: ["debian"]
draft: false
showTableOfContents: true
type: "post"
---
A few days ago, I was contacted by [IT for Change](https://itforchange.net) to make a video presentation on importance of [Free Software](/free-software) in education to teachers in Hindi. When I was making slides in LibreOffice Impress, I saw some problems with Hindi typing. First, I was not able to set the input language to Hindi and second, the fonts in LibreOffice Impress weren't rendering correctly.

So I asked my friend [Raghu](https://raghukamath.com) for help on how to type in Hindi and how to fix the issue, who helped me. I am documenting my findings here for future reference or helping other people.

I am using [PureOS](https://pureos.net) with KDE right now. The same should work in other GNU/Linux distros too, at least in Debian-based ones.

To type in Hindi, go to System Settings -> Input Devices -> Keyboard -> Layouts -> Add layout 

To add the layout, fill the following details: 

- Limit selection by language: Any language

- Layout: Indian  (They have written 'Indian' as a language, which should be 'Hindi') 

- Variant: default

- Label: in

Click 'Ok'. 

Tip: Ctrl + Alt + K keys is the shortcut for switching the input language. 

That sets up typing in Hindi.

<figure>
<a title="Michka_B (fr-Wikipedia), CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:KB_Devanagari_InScript_text_1.svg"><img width="512" alt="KB Devanagari InScript text 1" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/KB_Devanagari_InScript_text_1.svg/512px-KB_Devanagari_InScript_text_1.svg.png"></a>
<figcaption>Mapping of inscript Hindi keyboard</figcaption>
</figure>

Now for the problem of fonts, Raghu suggested me to download `fonts-lohit-deva` package which downloads Lohit Devanagari font. After downloading, run the command `fc-cache -v | grep lohit` to see if lohit is there. If it is there, we are good to go. 

In LibreOffice Impress, I changed the font to 'Lohit Devanagari' and the problem got fixed.

At last, I made the slides and recorded the video, which you can [watch here](https://videos.fsci.in/videos/watch/1c6f69c4-0842-4112-9e7b-2df489505aea).
