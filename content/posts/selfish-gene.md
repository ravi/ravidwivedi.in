---
title: "The Selfish Gene Book Overview"
date: 2022-04-08T11:39:20+05:30
draft: false
type: "post"
showTableOfContents: true
---
Yesterday I finished reading *[The Selfish Gene](https://en.wikipedia.org/wiki/The_Selfish_Gene)* by Richard Dawkins. The book reminds me of Daniel Kahneman's *Thinking Fast and Slow*, which explores how the human brain makes decisions. In contrast, The Selfish Gene puts forward how genes make decisions and manipulate our decisions for their own benefit, keeping in mind that 'selfish' is just a metaphor and a gene has no motive or will of its own. The book presents some very great ideas by other scientists and by the author himself, and for this reason I suggest you to read it. 

The larger the percentage of genes shared by the two individuals, the higher the chance of them behaving cooperatively towards each other. Each gene in a human body, and in many organisms from the animal kingdom, has a 50% chance of being in any of the person's sibling. The same goes for the parents, i.e, Each gene in a human body has a 50% chance to be in each of its parent's body and 50% chance of being in each offspring's body. Interesting questions raise from the gene point of view, why then a human body care more for their offspring than its siblings? Why there is an asymmetric relationship between parental care towards the offspring and offspring care towards the parent? After all, genetically, the investment in each individual of that organism is the same.

The book also asks whether parents should invest equally in all their offsprings? How offsprings compete for resources provided by the parent? Parents also try to minimize their effort and investment in the offspring and instead try to manipulate their mate to take more responsibility for the offspring. Why females have a sudden menopause, while males do not go through such a thing? 

There are even more interesting questions, such as, why we should look an organism as a whole unit? Why not consider a pack of wolves as a single unit? An organism, like me or you, is made up of complicated parts, each acting somewhat in unity. But what is so special about an organism to treat it as a single unit? Why did the genes or replicators chose to live in complicated bodies like animals?

The arguments and the selfishness surrounding them might make you cynical, but the author is optimistic that humans can be taught to be altruistic, and that gives us one more reason to teach children to be altruistic.
