---
title: "Trip to Prizren Bridge"
date: 2022-07-16
draft: false
type: "post"
showTableOfContents: true
tags: ["travel", "kosovo", "europe"]
---
Yesterday I went to [Prizren stone bridge](https://en.wikipedia.org/wiki/Old_Stone_Bridge,_Prizren), which is in Prizren, Kosovo. Prizren is often referred to as the cultural capital of Kosovo.

I am staying at the venue of [DebConf 22](https://debconf22.debconf.org/) from where it is walkable distance, around 1.5 km.

<figure>
<img src="/images/prizren-bridge.jpg" width="700px">
<figcaption>Prizren Stone bridge.</figcaption>
</figure>

I was going with my friends who were also attending the DebConf 22. We reached the bridge at around 20:30 hours time (Kosovo follows Central European Time). It is in the city center and the views were very beautiful. Lumbardhi river flows below the bridge with beautiful mountains surrounding on all sides.

{{< figure src="/images/sinan-pasha-mosque.jpg" title="Sinan Pasha Mosque" >}}

The bridge also gives a nice view of Sinan Pasha Mosque , which is a very important monument of the Prizren city, built in the Ottoman architecture style.

Near the bridge, a person was roasting corn which costed €1 per piece. The corn was soft, but did not have many kernels.

<figure>
<img src="/images/person-roasting-corn-at-prizren-bridge.jpg">
<figcaption>A street-side vendor roasting corn near the Prizren bridge.</figcaption>
</figure>

Found this beautiful take home souvenier in the shape of Prizren stone bridge.

<figure>
<img src="/images/stone-bridge-souvenir.jpg">
<figcaption>Souvenier from Prizren Bridge.</figcaption>
</figure>

