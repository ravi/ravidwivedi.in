---
title: "Thailand Trip"
date: 2024-03-22T02:15:00+05:30
draft: false
tags: ["thailand", "travel"]
showTableOfContents: true
type: "post"
---
**Note: 1 Thai Baht was equivalent 2.46 INR when I bought the Thailand's currency. However, it dropped to around 2.3 INR when I went there.**

## Arrival

I was staying at a hostel in Bukit Bintang, a shopping hub and entertainment center of Kuala Lumpur, which was like 200 metres from the monorail station, where I took a metro train for KL Sentral. Then I took a bus from KL Sentral to the airport, followed by taking a Malaysian Airlines flight to Bangkok. Meals were included without any extra charge.

The flight took around 2 hours, landing in Bangkok's Suvarnabhumi Airport at around 15:00 hours local time. My friend Fletcher was leaving for Pattaya from Bangkok, which is about 150 km and 2 hours bus ride, but takes a longer time from within the Bangkok city compared to Suvarnabhumi Airport due to the traffic in Bangkok. We had planned to meet in Pattaya and had already booked a hostel there. At the bus counter, I came to know that buses leave for Pattaya every hour. However, the next bus for which tickets were available was at scheduled to leave at 19:00 hours. The ticket cost was 143 Thai Baht (360 Indian Rupees) per seat. This left me with a couple of hours at the airport, which I spent charging my phone, exploring shops and eating snacks which I had packed. As I had one week to spend in Thailand, buying a SIM card was essential to have internet access on the go. The internet plans I checked at the airport were expensive compared to Kuala Lumpur, so I didn't buy a SIM card from there, leaving me with no way to coordinate with Fletcher.

{{< figure src="/images/malaysia-thailand/a-welcome-sign-at-suvarnabhumi-airport.jpg" title="Welcome sign at Bangkok's Suvarnabhumi airport." width="300" >}}

{{< figure src="/images/malaysia-thailand/suvarnabhumi-bus.jpg" title="Bus from Suvarnabhumi Airport to Jomtien Beach in Pattaya." width="300" >}}

## Pattaya

My bus left airport at 7 PM and dropped me at the last stop named Jomtien beach in Pattaya at around 9 PM. The bus journey was smooth, thanks to good roads and lack of traffic jam. I used OsmAnd app for navigation and decided to walk to my hostel as it was around 1 km away. While walking, I noticed Cannabis bars on the street side and ubiquitous massage parlors, not to mention the street prostitutes vying for your attention. After some time, I realized that the address mentioned on the hostel receipt was different from the one OpenStreetMap was showing, leading me to ask a person sitting at a café for help. That person agreed to help me and took me to the exact place where the hostel was. On the way, he told me he was originally from Kuwait but lived in Pattaya for many years. He also told me about the shared hail-and-ride cheap songthaew rides which ran along the Jomtien Beach, charging 10 Baht (25 Rupees) for any distance along that route, making our trip cost-effective.

{{< figure src="/images/malaysia-thailand/songthaew.jpg" title="Photo of a songthaew in Pattaya. These songthaews run along Jomtien Second road and take 10 Thai Baht to anywhere on the route." width="300" >}}

{{< figure src="/images/malaysia-thailand/jomtien-second-road.jpg" title="Road near Jomtien beach in Pattaya" width="300" >}}

I was at my hostel, but I was still not sure if I was at the correct place. There was no reception in sight, but only a staircase. In the meanwhile, I bumped into Fletcher, which gave me a sigh of relief. Adjacent to the property was a hairdresser who helped us get into the hostel. This was my first time checking in to a hostel without a reception. It seemed like a shared room without service. After some time, we went outside and bought a SIM card for me at a 7-Eleven store. The cost was 399 Baht for 7 day unlimited internet.

Next day, we went to Pattaya Tiger Park where you can go inside the tiger's cage and touch the tiger. Then we went to the nearby Floating Market but decided against going inside after reviewing ticket prices which were higher than we deemed worthwhile. After this, we walked towards the Jomtien beach, but I planned to hitchhike as it was very sunny, and our drinking water supply was getting depleted. A few cars passed by without responding, but a scooty stopped and picked us up. Initially, I thought he would let only one of us ride, but to my surprise he let both of us on it. During the ride, he told us he is from Delhi and dropped us at walking distance from the bus station where Fletcher had to board his bus for Bangkok.

{{< figure src="/images/malaysia-thailand/a-welcome-sign-at-Pattaya-floating-market.jpg" title="A welcome sign at Pattaya Floating market." width="300" >}}

{{< figure src="/images/malaysia-thailand/jomtien-beach.jpg" title="Jomtien Beach in Pattaya." width="300" >}}

In the evening, I went to a shop which was selling fruits like guavas, dragon fruits, mangoes. I sampled guavas, which I found tasty and got mangoes and dragon fruits packed for eating later, and they were yummy.

## Bangkok

Next day, on 9th Feb, I left for Bangkok, taking the bus at the bus station where the bus from Suvarnabhumi Airport dropped me the other day. I reached the airport in 2 hours and boarded a metro train from there to [Huai Khwang](https://en.wikipedia.org/wiki/Huai_Khwang_MRT_station), followed by walking to my hotel. This hotel room costed 5600 INR for four days for a double bed. In Bangkok too, I found abundance of convenience stores like 7-Eleven, similar to Pattaya and Kuala Lumpur. I had difficulty finding vegetarian food and labels were in Thai language, and the staff didn't know English.

{{< figure src="/images/malaysia-thailand/coffee-options-at-7-eleven.jpg" title="A board showing coffee menu at a 7-Eleven store along with rates in Pattaya." width="300" >}}

{{< figure src="/images/malaysia-thailand/premix-coffee-packets-at-7-eleven.jpg" title="In this section of 7-Eleven, you can buy a premix coffee and mix it with hot water provided at the store to prepare." width="300" >}}

Next day, on 10th Feb 2024 was Chinese New year. I didn't see a lot of celebrations in my area, but I saw celebratory programs and discount sales while I was in Kuala Lumpur, a few days before the Chinese New Year. In terms of food, I mainly relied on fruits. The pineapples were different from what I have seen in India, and this was the first time I saw banana flesh yellow. I tried Korean vegetable noodles, which were good. Another option was eating bread and cheese from the 7-Eleven. I also tried some Indian food like Chhole Kulche at Sukhumvit and some paneer dish at Ratchada Night market.

{{< figure src="/images/malaysia-thailand/thailand-banana.jpg" title="Banana with yellow flesh" width="300" >}}

{{< figure src="/images/malaysia-thailand/fruits-at-a-stall-in-thailand.jpg" title="Fruits at a stall in Bangkok" width="300" >}}

{{< figure src="/images/malaysia-thailand/pineapples-from-thailand.jpg" title="Trimmed pineapples from Thailand." width="300" >}}

{{< figure src="/images/malaysia-thailand/corn.jpg" title="Corn in Bangkok." width="300" >}}

{{< figure src="/images/malaysia-thailand/vegetasty.jpg" title="This Korean Vegetasty noodles pack was yummy and was available at many 7-Eleven stores." width="300" >}}

I explored malls in Siam and explored the area around Sukhumvit. The highlight of my Bangkok trip was the Chao Phraya Express Boat, which sails on the Chao Phraya River and covers some important tourist destinations of Bangkok. I took the all day river pass for the boat for 150 Thai Baht, which allows you to hop on and off any boat at any station for the day, only to realize later that it would have been better to buy respective station tickets to reduce the costs. I highly recommend taking the boat ride as it offers stunning views of the city. I went to Wat Arun, a famous Buddhist temple and a major tourist attraction. Upon entering the temple after buying the ticket, they stamped my hand ;)  Following this, I took a boat to the famous Khao San Road, which is famous for cheap accommodations and food stalls.

{{< figure src="/images/malaysia-thailand/wat-arun-stamp.jpg" title="Wat Arun temple stamps your hand upon entry" width="300" >}}

{{< figure src="/images/malaysia-thailand/wat-arun.jpg" title="Wat Arun temple" width="300" >}}

{{< figure src="/images/malaysia-thailand/khao-san-road.jpg" title="Khao San Road" width="300" >}}

{{< figure src="/images/malaysia-thailand/a-food-stall-at-khao-san-road.jpg" title="A food stall at Khao San Road" width="300" >}}

{{< figure src="/images/malaysia-thailand/chao-phraya-boat.jpg" title="Chao Phraya Express Boat" width="300" >}}

I had booked a flight from Bangkok to Delhi with Air India, and during the flight, they were serving alcohol onboard. I decided to try red wine, marking my first experience of consuming alcohol while flying.

{{< figure src="/images/malaysia-thailand/red-wine-in-air-india.jpg" title="Red wine being served in Air India" width="300" >}}

## Notes

- There are many malls in Bangkok and you can easily find toilets/restrooms, which can help to avoid paying for using washrooms.

- Compared to Malaysia, I found Thailand more expensive, contrary to expectations. In addition, shopping in Malaysia was quite cheap due to Chinese New Year discounts, which I didn't see in Thailand.

- I liked Pattaya more than Bangkok, mainly due to Pattaya having a nice beach and I could engage with locals and foreigners. Perhaps booking a hostel instead of a hotel in Bangkok would have given more opportunities to engage with people.
