---
title: "Announcement: I updated my gpg keys"
date: 2022-11-19T16:01:36+05:30
draft: false
type: "post"
showTableOfContents: true
---
I updated my gpg keys because I had a 3072 bits RSA key and [Debian requires 4096 bits RSA or elliptic curve](https://wiki.debian.org/Keysigning#Step_1:_Create_a_RSA_keypair). The key fingerprint is:

`FF7D B951 7CE1 E19B 6EFE 695F E0E5 BAFD 3BBF 70B3`

Check [my gpg page](/gpg) for details. 

So, if you are from Debian community and signing someone else's keys which they will be using for the purposes of debian, make sure they are using a 4096 bit RSA key or an elliptic curve key.
