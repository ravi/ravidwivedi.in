---
title: "Debian Packaging Tutorial: Removing Git From Gemspec"
date: 2023-04-05T01:48:53+05:30
tags: ["debian packaging", "debian", "tutorial"]
draft: false
showTableOfContents: true
type: "post"
---
Recently, I was packaging [silent_stream](https://rubygems.org/gems/silent_stream) for [Debian](https://debian.org). sbuild was showing error because there was git in gemspec file. Specifically, this line

	`git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(tests|spec|features)/}) }

in this [file](https://github.com/pboling/silent_stream/blob/master/silent_stream.gemspec#L51).

Thanks to Vinay who helped me in fixing this. It took some time and it is common to run into this error, I was told. So, I am documenting what worked for me.

We will use quilt and Rapahel has written a [good article](https://raphaelhertzog.com/2012/08/08/how-to-use-quilt-to-manage-patches-in-debian-packages/) on how to use it to patch. You will need to setup quilt first which is explained in the article.

---------------Run all these commands in debian unstable environment-----------------------------

1. Since I had already pushed my changes to salsa.debian.org, I had to clone the repository. We do this by using: 

	`gbp clone --pristine-tar git@salsa.debian.org:ruby-team/ruby-silent-stream.git`

	cd into the repository just cloned

	```cd ruby-silent-stream```

2. Now, insert a new empty patch

	```quilt new remove-git-in-gemspec.patch``` 

3. Tell quilt that you intend to modify files

	```quilt add remove-git-in-gemspec.patch``` 

4. Edit the file which requires to be fixed. In our case it is ```silent_stream.gemspec``` file

	```vim silent_stream.gemspec``` 

5. Delete lines 50,51,52 in this [file](https://github.com/pboling/silent_stream/blob/master/silent_stream.gemspec#L50-L52)

6. Replace those lines with:

	```spec.files = Dir.glob("**/*")```

	and save the file.

7. To generate the patch, run

	```quilt refresh```

8. Add [metadata](http://dep.debian.net/deps/dep3/) to your patch header

	```quilt header --dep3 -e```

9. Add ```debian/patches``` directory to git staging area:

	```git add debian/patches```

10. Restore gemspec file.

	```git restore silent_stream.gemspec```

11. Run ```debclean```

	`debclean`

12. Before running `sbuild`, I had to import tar files in parent directory

	`uscan --verbose -dd --download-current-version`

12. Check if ```sbuild``` is successful. 

	```sbuild -d unstable``` (where to run this command depends on how you setup sbuild and your debian unstable)

-----------------Run above commands in debian unstable environment------------

If the sbuild was successful, then commit and push your changes. I usually commit in my host system as signing the commits does not work for me inside chroot or the unstable environment I setup using [systemd-nspawn](https://wiki.debian.org/Packaging/Pre-Requisites/nspawn).

	git commit -S -m "add remove-git-in-gemspec.patch"

So, that's it for this tutorial. Meet you in the next post.
