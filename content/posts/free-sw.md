---
title: "What is free software and why is it important?"
date: 2021-07-26
---
(Authors: [Ravi Dwivedi](/), [Pirate Praveen](https://social.masto.host/@praveen))
showTableOfContents: true

(Edited on: 06-September-2021)

Check out my [Free Software page](/free-software) for resources and articles on the issue of Free Software.

#### Index

- [Definition](#definition)

- [Importance of Software Freedom](#importance-of-software-freedom)

    - [Freedom 0 - Freedom to run the software](#freedom-0---freedom-to-run-the-software)

    - [Freedom 1 - Freedom to study and modify the program](#freedom-1---freedom-to-study-and-modify-the-program)
    
    - [Freedom 2 - Freedom to share the software](#freedom-2---freedom-to-share-the-software)
    
    - [Freedom 3 - Freedom to share your modified versions of the software](#freedom-3---freedom-to-share-your-modified-versions-of-the-software)
    
- [Freedom in network-based services](#freedom-in-network-based-services)

- [How to check whether a software is free or nonfree?](#how-to-check-whether-a-software-is-free-or-nonfree)
    
#### Definition: 

Free Software means that the users have the freedom to:

- run the software as they wish (Freedom 0);

- study how the software works and adapt the software according to their needs, which means that the source code must be available to the user (Freedom 1);

- share the software with anybody, gratis (free-of-cost) or by charging a fee (Freedom 2); 

- share their modified versions of the software (Freedom 3).

<br>
<br>

![Free software respects users' freedom](/images/four_freedoms.png "Free software respects users' freedom").

<br>
<br>

Free Software does not mean that it is free-of-cost. For example, Conversations app available on Play Store is an example of a software [which respects your freedom but not available gratis](https://play.google.com/store/apps/details?id=com.dealerinspire.conversations&hl=en&gl=US). Some examples of free software are: VLC Media Player, Firefox browser, Ubuntu etc.

Any software which lacks any of the above mentioned freedoms is called a non-free software or proprietary software. Some examples of nonfree/proprietary software are: Microsoft Windows, Safari browser, Google Chrome, Adobe Photoshop. 

This article has nothing to do with price, so the term 'free' will always mean liberty/freedom.

#### Importance of Software Freedom

Let's look at each freedom in detail see why they matter as much that they should be (as an ethical issue) every software user's rights. 

#### Freedom 0 - Freedom to run the software

Have you ever seen lengthy terms and conditions before using the software? 

We are sure you have.

But, have you really read these before clicking 'I Agree'? 

Usually these terms and conditions restrict users to use the software and [they can be very dangerous](https://www.eff.org/wp/dangerous-terms-users-guide-eulas). For example, Microsoft's nonfree FrontPage's license [included terms that prohibit use of the software](https://web.archive.org/web/20030515043511/http://archive.infoworld.com:80/articles/op/xml/01/10/01/011001opfoster.xml) to criticize Microsoft. 

Another example is Pinnacle's Studio 9 movie-making software which includes a condition [to allow Pinnacle to install software automatically from third parties onto your computer](https://www.eff.org/wp/dangerous-terms-users-guide-eulas) – software which the vendor admits may "impair" the program ("the Software") you have just purchased, as well as "any other software on your computer which specifically depends on the Software." 

In contrast, Free Software licenses like [GNU GPL](http://gnugpl.org/) only list what rights users have but don't restrict them to use the software. For true liberty, you must have the freedom to run the software as you wish. The developer should never get to decide how the users can use the software. 

#### Freedom 1 - Freedom to study and modify the program

If you cannot study the program, you cannot learn from it. Moreover, you can never know what the program does. On the surface, it might be an innocent looking video game but [in reality it might be a spyware recording your whole life](https://www.thestar.com/news/canada/2015/12/29/how-much-data-are-video-games-collecting-about-you.html). Furthermore, if you cannot modify the software, you cannot remove malfunctionalities, fix any bugs, or simply adapt the program according to your needs. This makes you dependent on the owner of the software and at their mercy that they won't mistreat you. 

Example of exercizing freedom 1 in free software:  

A free software named [Tux Paint](http://directory.fsf.org/wiki/TuxPaint) used at VHSS Irimpanam school, Kerala, where 11 and 12 years old students completely translated the program's interface to Malayalam, and added pictures of local flowers along with recordings of Malayalam pronunciations of their name to the program's library. While doing this, they exercised their freedom to learn how the program works and modify the program, which demonstrates that even non programmers or children, [can actually directly improve software](https://www.gnu.org/education/edu-software-tuxpaint.html) when software freedom is granted.

The examples of free software modifications are all over the place. Tor Browser is a fork of Firefox browser, Conversations app has many modifications, like Blabber, Quicksy etc. LibreOffice is a fork of OpenOffice. It is a very common thing in free software communities. 

Another benefit of providing users the source code is that people can learn from the source code of a program (like theorems in mathematics). Also, people don't need to develop software everytime from scratch. They can modify the code of the existing software and adapt according to their needs. Imagine, if every software lacks freedom 1, then everyone would need to develop software from scratch, a lot more redundant task in comparison to forking or learning from already existing software.

Nonfree software usually have [backdoors](https://www.gnu.org/proprietary/proprietary-back-doors.html), [spywares](https://www.gnu.org/proprietary/proprietary-surveillance.html#content) and is [addictive, controlling and manipulative](https://observer.com/2016/06/how-technology-hijacks-peoples-minds%E2%80%8A-%E2%80%8Afrom-a-magician-and-googles-design-ethicist/). Moreover, users cannot even remove these malfunctionalities. 

Nonfree software, which lack Freedom 1, [mistreat users](https://gnu.org/malware) and users are helpless because they don't have source code of the program. If users have the source code, they can remove the malfunctionalities themselves. 

Also, with nonfree software which lack freedom 1 like Microsoft Windows, you cannot have security from the owner of the software itself, which is Microsoft (not you!). Only Microsoft can make changes in the Windows and you cannot. Even if you know of a vulnerability in the program and you have the ability to fix it, you will have to wait for Microsoft to fix the bug. This makes you completely dependent on Microsoft for the usage of this software.

#### Freedom 2 - Freedom to share the software

In his article [Freeing the Mind : Free Software and the Death of Proprietary Culture](http://moglen.law.columbia.edu/publications/maine-speech.html), Eben Moglen raises a very important moral issue: 

"If I can provide to everyone all goods of intellectual value or beauty, for the same price that I can provide the first copy of those works to anyone, why is it ever moral to exclude anyone from anything? If you could feed everyone on earth at the cost of baking one loaf and pressing a button, what would be the moral case for charging more for bread than some people could afford to pay?"

Software companies like Microsoft criminalize the sharing of the software which they develop. They call it "piracy" when the users share software with each other. These practices are exclusionary. The point is why exclude anyone from having a copy of the software when copying is as easy as a click and there is no marginal cost for a copy. Technology has enabled users to copy files without any marginal cost. What is the benefit of the technological progress if you cannot even use it? 

Yes, there is some development cost of every software but there can be other ways to cover this cost and earn money rather than prohibiting users from sharing copies. 

#### Freedom 3 - Freedom to share your modified versions of the software

Modification of the software can usually be used to fix bugs, remove any malfunctionalities, or adapt the software according to the user's needs/likings. 

We depend on plumbers to fix our taps, lawyers to handle our legal cases, mechanics for repairing our bikes, cars etc. In a similar way, we can give our software customizations tasks to technical people (maybe, in exchange for a fee) to adapt the program for us (Freedoms 2 and 3). If we don't have freedoms, then this customization isn't even possible. Good luck begging the developer to add features that you like. 

For example, freedom 3 ensures that

- if one person removes the malfunctionalities from the software, other users of that software don't have to put in redundant work to remove the same malfunctionality themselves;

- if one user adapts the software to their needs, they can share the modified versions with others who agree with the adaptation. This also reduces the redundant work that every person might have to do if the freedom 3 is not granted;

- if one user fixes the bugs, they can send it to the developer and this will be useful to all the users;

- people who cannot modify software themselves, like nontechnical people (say, for example, people who do not know programming), technical people who do not know that programming language in which the source code is released or simply don't have time to modify themselves. 

Take the example of Scribus, which is a cross-platform (works on GNU/Linux, Microsoft Windows, macOS, etc) Free Software for Desktop Publishing (replacement for softwares like Adobe PageMaker and InDesign). It initially only had support for latin languages. Later, the developers of a community project funded by the Oman Government, called HOST-Oman (House of Open Source Technologies - Oman), introduced support for Non-Latin languages(they wanted Arabic support) with Complex Text Layout in Scribus. And as a part of this development, Malayalam also got supported in Scribus. This enabled [Janayugom newspaper to shift to 100% Free Software for their publishing](https://poddery.com/posts/4691002). So we can see that anyone improving a software turned out to be beneficial to all the users even if they didn't improve the software themselves. 

Free Software is important for your liberty, privacy and security. And with the increasing dependence of our lives on software day-by-day, for all kinds of work, free software has become even more important. 

#### Freedom in network-based services

The above four-freedom definition works well for software installed in your system. For software which requires a server for its full functioning (like some chatting apps, social media platforms, search engines), only the software freedom is not enough, in my opinion. 

For example, Telegram's client software is free software but the [server side is proprietary](https://telegram.org/faq#q-can-i-get-telegram-39s-server-side-code), i.e.,in control of the central Telegram servers. 

Signal is another such chatting system (see note 1 at the bottom of the post) (software + server) whose software(app which you install in your system) is free software. Signal's server-side code is also free. This means it can be self-hosted. For example, a company or an organization like Kerala government can run a Signal server and a modified Signal app for their employees/workers. But these workers/employees cannot communicate with people on central server/other servers of the Signal system. 

When LibreSignal, a fork of Signal, was developed (by modifying its source code), Signal's developer [refused to allow LibreSignal to use its servers](https://web.archive.org/web/20160914124807/https://github.com/LibreSignal/LibreSignal/issues/37#issuecomment-217211165) and [refused to federate with them](https://web.archive.org/web/20160914124807/https://github.com/LibreSignal/LibreSignal/issues/37#issuecomment-217231557), which means, if LibreSignal developer wants to run their own server, users from LibreSignal's server cannot connect to users registered on Signal's central servers. (A good article on this incident is [here](https://web.archive.org/web/20160914124807/https://lwn.net/Articles/687294/).)[1]

In view of this, I think that the freedom in network-based services should mean that you have the freedom to self-host the service on your server and federate with other servers of that system. Further, if bridges and interoperability with other standards are possible, that would provide more freedom. For example, Matrix allows  Telegram, IRC, XMPP users to post in Matrix rooms and Matrix users to post in Telegram channels, XMPP rooms, and IRC channels using bridges. Currently, bridges work only in unencrypted rooms and the room admins need to enable the bridge manually from both the sides of the bridge.

In some cases, the concept of federation does not apply and only the freedom to self-host is sufficient, for example: search engine, videoconferencing etc. In chatting systems, social media etc, federation is essential to freedom. 

Examples of self-hostable systems (definition of federation does not apply here) : 

- Jitsi Meet is a free software for video conferencing.

- Searx is a free software metasearch engine (takes input from a user and uses other search engines for results) which acts as a proxy (hides users from these services).

Examples of federated systems are:

- Mastodon is a free software social media which is federated. Regardless of which service provider the user has registered with on Mastodon, they can talk to any other Mastodon user. 

- PeerTube is a free software video uploading and viewing platform. PeerTube channels can be followed from a Mastodon/Pleroma/Fediverse account (because both PeerTube and Mastodon are based on ActivityPub), comments/likes/reshare of PeerTube videos can be done to via Mastodon/Pleroma/Fediverse account.

- Matrix, XMPP are federated chatting systems.

With federation and freedom of self-hosting, even if you don't run your own server, you can change your service provider if you do not agree with their terms or privacy policy. There is no single point of failure. Centralized services, means controlled by a single entity, could be more susceptible to legislation requiring backdoor access. They also hold full control over the service and can use this power to impose any restrictions on you in the future. 

**TL;DR:  Four freedoms - to run, to study the code, to modify, share the client software + self-hostable server software + supporting federation of services by using a free standard protocol gives users full collective control over the network-based services.**

To achieve these freedoms, it is very essential to [build communities](/community-run-services) and work together in collaboration to maintain such services. Also, since a lot of nonfree software, like WhatsApp, are used because of the network effect (because others are using WhatsApp), campaigning for free software and wider adoption of free software has become more important now.

Free Software is necessary (but not sufficient) to create a free society where people have liberty. We would like to create a better society and so we campaign for free software.  

#### How to check whether a software is free or nonfree?

Check the license of the software and match it with [this list of free software licenses](https://www.gnu.org/licenses/license-list.html#SoftwareLicenses). If the license under which the software is released is in the list, then it is free software, otherwise it is not.

For example, let's check the license of Conversations app. To do this, visit their [website](https://conversations.im) and then go to Development section at the top. There is a [Github link](https://github.com/inputmice/Conversations) in that section. The Github page has a license file and if you click that, you will reach [this URL](https://github.com/iNPUTmice/Conversations/blob/master/LICENSE). It lists the license as GNU General Public License v3.0 which is in [this list of free software licenses](https://www.gnu.org/licenses/license-list.html#SoftwareLicenses). Therefore, we conclude that it is free software.

Another method to check if an app is free software is: If it is avaialble of [F-Droid](https://f-droid.org), then it is free software. 

<br>

[1] : The comparison of Telegram and Signal is a bit complicated because Telegram allows bridges through which a Telegram channel can be connected with a Matrix room. Telegram's server side is proprietary but Signal's server side is free.
