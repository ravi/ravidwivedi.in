---
title: "Malaysia Trip"
date: 2024-03-02T19:29:59+05:30
draft: false
type: "post"
showTableOfContents: true
tags: ["malaysia", "travel"]
---
## Introduction

In January of this year, my friend Snehal invited me to Vienna, Austria, but our plans were thwarted when Austria refused me a visa. So, I thought about traveling to a country that does not require a visa, and a few candidates came to mind, one of them being Malaysia which had recently [waived](https://www.thehindu.com/news/national/malaysia-joins-thailand-and-sri-lanka-in-granting-visa-free-entry-for-indians/article67579107.ece) visa requirements for Indian tourists. I also included Thailand, as it was visa-free, near to Malaysia, and is one of the most popular tourist destinations in the world.

I booked an AirAsia flight from Delhi, India, to Kuala Lumpur, Malaysia; a Malaysia Airlines flight from Kuala Lumpur to Bangkok; and an Air India ticket from Kuala Lumpur to Delhi.

## Boarding from the Delhi Airport

On the 31st of January, before boarding my flight to Kuala Lumpur, I went to the AirAsia counter to obtain my boarding pass. During this process, the airline staff asked me a range of questions, from the usual - such as my purpose of visit, hotel bookings, and return ticket - to the ridiculous, including requests for invoices of hotel bookings, email confirmations of my return ticket, and the amount of currency I was carrying.

I didn't have the invoices of my bookings as I didn't pay for them in advance except for the first night, nor could I find the confirmation email by Air India. I was afraid that the airline staff might not give me the boarding pass, which is required to board the flight. I never had such experiences at airports in other countries. 

Then I passed through the immigration where I was asked sensible questions such as my country of visit and return tickets.  Following this, I went through security check, and proceeded to the flight's boarding gate. Unfortunately, my flight was delayed by three hours. Even though AirAsia is a budget airline, I was surprised to find out that drinking water inside the plane was not free-of-cost, only hot drinking water was. It was a direct flight, which took 5 hours to reach Kuala Lumpur. I landed at Kuala Lumpur's KLIA2 airport at around 09:00 hours local time (which is 2 hours 30 minutes ahead of Indian time) on the 1st of Feb.

## Day 1: Arrival in Kuala Lumpur

After arrival at the Kuala Lumpur airport, I went through immigration where I was asked my purpose of visit, number of people accompanying me, number of days of stay, return/onward tickets and hotel bookings. 

{{< figure loading="lazy" src="/images/malaysia-thailand/klia2-airport.jpg" title="Kuala Lumpur International Airport." width="500">}}

I had filled [Malaysia Digital Arrival Card](https://imigresen-online.imi.gov.my/mdac/main?registerMain) a day before my flight's scheduled departure as per my travel agent's advice, though the immigration officer didn't ask for it. The immigration officer stamped my passport, after which I went through custom clearance. After clearing customs, I roamed around the airport and checked out SIM and internet plans, without buying.

{{< figure loading="lazy" src="/images/malaysia-thailand/malaysia-passport-stamp.jpg" title="Malaysian entry stamp on my passport." width="500">}}

Malaysia’s currency is the Malaysian ringgit, abbreviated as MYR. One MYR was equivalent to 18 Indian Rupees (INR). I exchanged Malaysian ringgit at a money exchange in my hometown.

My stay was booked at a hostel called [Travel Hub Guesthouse](https://omaps.app/_0GA7K57rM/The_Travel_Hub) in the Chinatown area of Kuala Lumpur, which was within walking distance of the [Pasar Seni metro station](https://www.openstreetmap.org/way/141466517), known as LRT station in local jargon. Every booking included a MYR 10 tourism tax per night. The hostel stay was 26.71 MYR plus 10 MYR (taxes), adding up to a total of 36.71 MYR (660 INR) per night.

 KL Sentral serves as the city center and is located about 50 km from the airport. The rapid train from the airport to KL Sentral costs 55 MYR (approximately 1,000 INR), which I found to be quite expensive. Instead, I opted for the bus, which was a more economical choice at 15 MYR (approximately 270 INR). The bus took about an hour to reach KL Sentral and had comfortable seats. The well-maintained roads made my overall journey smooth and offered a glimpse of Kuala Lumpur city.

{{< figure loading="lazy" src="/images/malaysia-thailand/bus-from-airport-to-kl-sentral.jpg" title="Interior view of the bus." width="500">}}

As soon as I checked in and entered my room, I met an Indian named Fletcher, who was also a tourist, and been in Kuala Lumpur for a couple of days, was planning to visit the National Museum and I joined him out of excitement for exploration, even though I was too tired after such a long trip and wanted to rest instead.

{{< figure loading="lazy" src="/images/malaysia-thailand/lrt.jpg" title="LRT at KL Sentral." width="500">}}

{{< figure loading="lazy" src="/images/malaysia-thailand/pasar-seni-lrt-station.jpg" title="Entrance of Pasar Seni LRT station" width="500">}}

{{< figure loading="lazy" src="/images/malaysia-thailand/travel-hub-guesthouse-room.jpg" title="Room inside Travel Hub Guesthouse." width="500">}}

In the evening, I visited the National Museum (ticket was 5 MYR, equivalent to 90 INR) with Fletcher, and explored Little India, which had abundance of Indian food restaurants, especially by Tamils, making vegetarian food easily available. I came across a stall where I drank masala tea and sampled Mee Goreng. A quick search on the internet revealed that Mee Goreng is a dish unique to Indian immigrants in Malaysia and neighboring countries, but not found in India!

{{< figure loading="lazy" src="/images/malaysia-thailand/little-india-welcome-board.jpg" title="Board welcoming us to Little India" width="500">}}

{{< figure loading="lazy" src="/images/malaysia-thailand/mee-goreng.jpg" title="Mee Goreng, a dish made of noodles in Malaysia." width="500">}}

## Day 2: Visiting Batu Caves and Petronas Towers

To stay with Fletcher, I extended my stay at the same hostel for one night. The next day (February 2nd), Fletcher went on a day trip to the Genting Highlands. Although I planned to join him, I couldn’t because his bus had no available bookings when we reached the KL Sentral station the next day. I noticed that at the KL Sentral station, bus tickets need to be paid for using a card or can be booked online through websites like RedBus. They had a cash option at the KL airport, but not at KL Sentral.

I took the opportunity to buy a local SIM card from the company CelcomDigi at the KL Sentral station for MYR 10, which included 5G internet up to 5 GB of usage. However, making calls required a recharge of MYR 5, which I didn’t include. At the bus ticket counter, I met a family from Delhi and joined them for a day trip to Batu Caves, which is a cave complex located on the outskirts of Kuala Lumpur.

To reach Batu Caves, we took the KTM Komuter train from KL Sentral station, which costs MYR 5.2 for a return journey. The train takes around 40 minutes to reach Batu Caves. Upon reaching there, we could immediately see the shrine of Murugan outside the Batu Caves. There were stairs leading to the caves. In order to take the stairs to the caves, make sure you cover your knees and shoulders. A woman from the family I went with was wearing shorts, so she had to buy a scarf worth MYR 15 to cover her knees for the entry. After climbing the stairs, we went inside the Temple Cave. This particular cave doesn’t have an entry fee, however some other caves (for example, the Ramayana Cave) have an entry fee. I didn’t go to the Ramayana Cave because I didn’t know about it.

{{< figure loading="lazy" src="/images/malaysia-thailand/KTM-Komuter.jpg" title="KTM Komuter train" width="400" >}}

{{< figure loading="lazy" src="/images/malaysia-thailand/murugan-shrine.jpg" title="Murugan statue outside of Batu Caves." width="400" >}}

{{< figure loading="lazy" src="/images/malaysia-thailand/view-from-top-of-stairs.jpg" title="View from the top after climbing all the stairs." width="400" >}}

{{< figure loading="lazy" src="/images/malaysia-thailand/temple-inside-cave.jpg" title="Temple inside the cave." width="400" >}}

The temperature inside the cave was cooler. We spent some time resting inside the cave. After we were done, we returned to KL Sentral, and went our separate ways. I came back to hostel and rested for some time.

My trip to Kuala Lumpur would be incomplete without a photo at the iconic Petronas Towers, which was my next destination. To get there from my hostel, I took the LRT to KLCC station and then walked to the photo point of the Petronas Towers, where I asked an Indonesian tourist for help in taking my pictures.

{{< figure loading="lazy" src="/images/malaysia-thailand/me-at-petronas-towers.jpg" title="Me at Petronas Towers." width="400" >}}

After roaming around at Petronas Towers, I gave a phone call to Fletcher who was on his way back to KL Sentral and we decided to meet there. We went to the same stall we had Mee Goreng the previous day and ordered Ghee Roast Dosa this time. We also had nice conversations with a family having Malaysian citizenship with Indian ancestry. Then, we went to another place to eat Roti Canai, which I had with dal. Interestingly, Roti Canai is another dish popularized by Indian immigrants in Southeast Asia, but it is not popular in India.

{{< figure loading="lazy" src="/images/malaysia-thailand/photo-with-malaysians.jpg" title="Photo with Malaysians." width="500" >}}

## Day 3: Berjaya Times Square and Bukit Bintang

For the third day (3rd Feb), I could not extend my hostel booking as it was fully booked due to weekend. So, I booked another hostel by the name of [The Manor by Mingle](https://www.openstreetmap.org/node/11581359149) for two nights, 1 km far from my previous hostel, which was MYR 99.34 (1800 INR) for two nights, including MYR 10 tourism fee per night. This was expensive compared to other hostels I stayed in this trip, probably due to weekend. It had a swimming pool, which was of no use to me. Further, it also had laundry services, which I used. The charges were MYR 5 for washing, while MYR 3 for the dryer.

After checking-in and keeping my luggage into my room, I and Fletcher went to nearby shopping mall Berjaya Times Square, which was decorated in celebrations of the upcoming Chinese New Year on 8th Feb, due to which prices were greatly discounted. After roaming around and a bit of shopping, we went back to our respective hostels to take rest. At night, we went to Bukit Bintang, which is known for its nightlife and called the entertainment hub of the city.

{{< figure loading="lazy" src="/images/malaysia-thailand/berjaya.jpg" title="Berjaya Times Square dipped in Chinese New Year celebrations." width="500">}}

## Day 4: Genting Highlands

On the 4th of Feb, I took a solo day trip to [Genting Highlands](https://en.wikivoyage.org/wiki/Genting_Highlands), a hill station located on the outskirts of Kuala Lumpur. To reach there, I took a bus from KL Sentral (return ticket was MYR 20), which dropped me at the bus terminal below Awana Skyway cable car station. I took a cable car to reach Genting Highlands from there (MYR 18 for a return ticket), which passes through misty air along with stunning views. I roamed around and did some shopping, buying three T-Shirts for myself. Furthermore, I also sampled Paneer Makhani with naan for MYR 41.8 at a restaurant.

{{< figure loading="lazy" src="/images/malaysia-thailand/cable-car-1.jpg" title="Views from cable car." width="500">}}

{{< figure loading="lazy" src="/images/malaysia-thailand/cable-car-2.jpg" title="Views from cable car." width="500">}}

{{< figure loading="lazy" src="/images/malaysia-thailand/paneer-makhani.jpg" title="I ordered Paneer Makhani with naan at a restaurant in Genting Highlands." width="500">}}

There was not much to do here for me, as this place was popular for being the only legal place to gamble in Malaysia, nothing of my interest. Although the cable car ride had scenic views, I don’t think Genting Highlands was worth visiting. Sure, it’s a good place for a day trip from KL, but I had more time and should have instead gone to some other place like Cameron Highlands. Genting Highlands is for people staying for a couple of days in KL who don’t want to visit far-off places. I think my decision to visit Genting Highlands was a case of falling into the trap of herd mentality, as when I went to the bus counter a couple of days ago, I saw a big queue for Genting Highlands, which was the basis of my decision.

I took the return cable car to reach the bus terminal, from where I took the bus for KL Sentral. Then I went to meet Fletcher, who was leaving for Bangkok. We later met in Pattaya, Thailand, after a few days, which will be covered in the next post.

## Days 5 and 6

Since the hostel Manor by Mingle was a bit expensive, I booked a cheaper hostel for last two nights in Bukit Bintang. I didn't really do much on last two days. I roamed around and added some places on the OpenStreetMap. As I was living in Bukit Bintang, I bought some souveniers from there and tried a middle-eastern dessert named [Kunafa](https://en.wikipedia.org/wiki/Kunafa), which was yummy. I also discovered a shop named 'I Love KL Gifts' which had souvenirs at a great price.

{{< figure loading="lazy" src="/images/malaysia-thailand/kunafa.jpg" title="Kunafa, a middle-eastern dessert I sampled in Bukit Bintang." width="500">}}

## 7th Feb: Malaysian Airlines to Bangkok

On the 7th of Feb, I took a Malaysian Airlines flight for Bangkok, which was scheduled to depart from KL Airport at 12:15 hours, but the departure got delayed by 2 hours, landing in Bangkok at around 15:00 hours local time (which is 1 hours 30 minutes ahead of Indian time). More details will be covered in the Thailand post.

{{< figure loading="lazy" src="/images/malaysia-thailand/malaysian-airlines.jpg" title="Malaysian Airlines jet standing at KL Airport." width="500" >}}

## Expenses

| Category       | Amount (INR) |
|------------------------|---------------|
| Food + Accommodation + Travel in Malaysia | 10,000        |
| Delhi to Kuala Lumpur flight | 13,000        |
| Kuala Lumpur to Bangkok flight | 10,000        |
| Bangkok to Delhi flight | 12,000        |


## Learning for future trips

Malaysia has numerous places worthy of a visit: the Cameron Highlands (for its beautiful tea gardens), Malacca (due to its unique history and culture), Langkawi (for its white sand beaches) – and this list does not include the scenic spots from the part of Malaysia on the island of Borneo. However, I limited my trip to Kuala Lumpur and nearby places, which was a bad idea. After the trip, I realized that my mobility was reduced because I was carrying a trolley bag. Additionally, my trip to the Genting Highlands could have been replaced by a better alternative.

If you are like me and avoid taking a lot of taxis, preferring to walk or use public transport whenever possible, I highly advise you to cut back on luggage. Another instance where I could have benefited from this was with the airline ticket from Kuala Lumpur to Bangkok, which would have been much cheaper if I had carried less luggage (the tickets were 3,000-4,000 INR), compared to the ticket I bought for 10,000 INR.
