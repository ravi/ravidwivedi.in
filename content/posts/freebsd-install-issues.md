---
title: "FreeBSD Install: Hiccups and Issues"
date: 2023-07-31T19:44:19+05:30
tags: ["FreeBSD"]
draft: false
showTableOfContents: true
type: 'post'
---
I installed FreeBSD 13.2 with KDE today on my Dell Inspiron 5482. I have heard good things about FreeBSD primarily as an operating system for servers, but the project claims that it works well for desktop users as well. So, I thought I will give it a try. There were some things I had to figure out during the installation which I will note in this post for future reference. And I had to uninstall FreeBSD within hours of installation, due to issues I faced so it didn't went well. I am going to list them here and how I figured out some things.

## Regdomain selection during install

During install, the installer asked me to select a regdomain. I figured out that the default option -- FCC/United States of America worked for me.

## Xorg didn't startup

Post installation, I installed xorg in FreeBSD and added a file ```/usr/local/etc/X11/xorg.conf.d/20-intel.conf``` as per the example 1 in the [official handbook](https://docs.freebsd.org/en/books/handbook/x11/#x-config-video-cards). But ```startx``` command wasn't working, so I ran the command:

```Xorg -configure```

and  then 

```cp /root/xorg.conf.new /usr/local/etc/X11/xorg.conf``` followed by editing the file ```/usr/local/etc/X11/xorg.conf``` by changing Driver value to ```"i1915kms"``` under the Section "Device".

## Managing brightness during startup

When using FreeBSD, my keyboard's brightness buttons were not working. To decrease brightness, I used the preinstalled backlight utility.

```backlight -f /dev/backlight/backlight0 10``` 

Credits to [this answer](https://superuser.com/a/1702828).

I couldn't manage the brightness to be automatically low at startup. It was at 100% after very time I booted.

## Crashed frequently

The system crashed frequently and wasn't stabled at all. It automatically reboots after some time. I tried to debug and fix, but couldn't. So, for now I have uninstalled FreeBSD.

If you know any of the fixes, please let me know.
