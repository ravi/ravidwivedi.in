---
title: "Is GNU/Linux hard to use for beginners?"
date: 2022-06-28T17:47:49+05:30
draft: false
type: "post"
showTableOfContents: true
---
GNU/Linux operating systems (if you heard about Linux operating systems, I am talking about the same. Read [Why GNU/Linux](https://www.gnu.org/gnu/why-gnu-Linux.html) if you are curious about the details.) have a reputation of being hard to use for non-techies. Some of the popular examples of GNU/Linux distributions are: Ubuntu, Debian, Fedora etc. I repeatedly hear that they are hard to install or knowledge of command line is necessary to use them and therefore only advanced users use GNU/Linux. This is not true. Depending on your requirements, you can choose a distro which does not require a lot of technical skills to use.

First of all, I would like to point out why I advertise GNU/Linux operating systems. I promote the idea of [Free Software](/free-software) and when I suggest people to use freedom-respecting software, the obvious choice for a desktop operating system is a GNU/Linux operating system. There are many non-GNU Linux based operating systems as well and I am fine with people using them, but since I never used them, I suggest what I have used. 

What I think is that GNU/Linux is for everybody. There is so much choice here that it is practically never ending. Whether you want to use as a normal user who just want a stable operating system or you are someone who likes to hack around and break things while learning something new, GNU/Linux is for you. Proprietary operating systems are [full of malware](https://gnu.org/malware), so it is good time to switch to a GNU/Linux distro, which are usually built by a community, everything is public and open for all to see, respect users' freedom and privacy, plus they are usually [far more secure than proprietary software](https://en.wikipedia.org/wiki/Security_through_obscurity), by design. 

Whether it is school labs, a company's production machine, personal computers, servers, Raspberry Pi, human right defenders, journalists, GNU/Linux is for everybody. The choice of the GNU/Linux distro depends on your use case and threat model. It is not like Windows that everyone is using the same thing with same appearance. Different people choose different distributions with very different appearances and customization, depending on their taste.

### How to choose a GNU/Linux distribution for yourself

There is a huge number of GNU/Linux distros to choose from that it can be overwhelming to research all the choices. [Librehunt](https://librehunt.org/) website can ease your search by asking a few questions on what type of distro you want and then suggests you with a few distros matching your criteria. Popular GNU/Linux distributions like Ubuntu, Debian, Fedora, has a lot of user support by the community. If you use Ubuntu, for example, and you are stuck with some technical problem, chances are it is already answered on their [Askubuntu forum](https://askubuntu.com/). If not, then you can ask your question there. Many Free Software communities provide technical support. GNU/Linux distros usually have large communities behind them and work on the operating system of their choice in collaboration. Here are a few suggestions of mine for a user-friendly distro to start with: Debian, Ubuntu, Zorin OS, GNU/Linux Mint, Fedora. I haven't tried all of them but I have heard good feedback about Zorin OS, Linux Mint and Fedora.

After you have selected a distro, the next step is to choose a Desktop Environment. These differ in their appearances and philosophy. For example, if you are planning to run GNU/Linux on old hardware, then XFCE is a suitable Desktop Environment, which is lightweight and does not consume a lot of resources. GNOME and KDE Plasma are very popular Desktop Environments with large communities behind them.

<figure>
<a title="Граймс, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Debian_11_with_GNOME_desktop.png"><img width="512" alt="Debian 11 with GNOME desktop" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Debian_11_with_GNOME_desktop.png/512px-Debian_11_with_GNOME_desktop.png"></a>
<figcaption>Debian 11 with GNOME desktop. Source: <a href="https://commons.wikimedia.org/wiki/File:Debian_11_with_GNOME_desktop.png">Wikimedia Commons</a>.</figcaption>
</figure>

<figure>
<a title="KDE, GPL &lt;http://www.gnu.org/licenses/gpl.html&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:KDE_Plasma_5.21_Plasma_dark_screenshot.png"><img width="512" alt="KDE Plasma 5.21 Plasma dark screenshot" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/KDE_Plasma_5.21_Plasma_dark_screenshot.png/512px-KDE_Plasma_5.21_Plasma_dark_screenshot.png"></a>
<figcaption>KDE Plasma desktop. Source: <a href="https://commons.wikimedia.org/wiki/File:KDE_Plasma_5.21_Plasma_dark_screenshot.png">Wikimedia Commons</a>.</figcaption>
</figure>

### Installation

The installation of a distro depends on your hardware and the distro you choose. The distro you are installing might have installation guide on its website. For many distros, the installation is kind of the same. Arch Linux is for a bit advanced users where the installation differs from, say Ubuntu. 

You will find tons of blogs and video tutorials to install distro of your choice. 

Many laptops ship with GNU/Linux preinstalled. Some examples are: Indian vendor [Mostly Harmless](https://mostlyharmless.io/), [Purism devices](https://puri.sm/),  [Ministry of Freedom](https://minifree.org/) project in the UK, Lenovo selling laptops with [Ubuntu pre-installed](https://news.lenovo.com/pressroom/press-releases/lenovo-launches-Linux-ready-thinkpad-and-thinkstation-pcs-preinstalled-with-ubuntu/) and [Fedora pre-installed](https://fedoramagazine.org/lenovo-fedora-now-available/), Clevo selling laptops with [Debian pre-installed](https://laptopwithLinux.com/buy-debian-laptop-notebooks-with-debian-Linux-preinstalled/), Manjaro has also partnered with some hardware manufacturers to ship with [Manjaro out of the box](https://manjaro.org/hardware/), [System 76 laptops](https://system76.com/laptops), etc.

I suggest you to do some research and choose a computer with GNU/Linux preinstalled for the next time.

### Learning about the GNU/Linux operating system

Many blogs and Youtube channels exist where you can learn about the distros. If you are curious to learn the command line, you will find tons of resources on the internet for learning. There are many books written on the command line which you can choose for your learning. Whatever distro you are using, [Arch Wiki](https://wiki.archLinux.org/) can be a handy guide.

I started with Debian as a beginner and I learnt some command line just for fun and curiosity, but it is not mandatory for a Debian user. I found the operating system easy and intuitive to use. I have used Manjaro for a couple of months and PureOS for around half a year. Whenever I am stuck, I usually get help from the internet.

### Conclusion

All in all, GNU/Linux distros are highly customizable and give users choice and freedom to use what they want. There are user-friendly distros like Ubuntu and Debian with good user support and large communities behind them so that you can always ask for help. I hope this will post will strike down any fear or doubts you might have in starting using GNU/Linux.
