---
title: "My Experience of running Snikket"
date: 2022-01-20
draft: false
type: "post"
showTableOfContents: true
---
### What is Snikket?

[Snikket](https://snikket.org) is a software that can be installed on a server to self-host an xmpp server. Snikket makes some choices for the person installing it on the server and therefore, makes it easy to get the features any XMPP server would usually like to have-- audio/video calls, file sharing, etc. It is very convenient to set up and saves time. The downside is that you lose control over your setup, but you can choose to install plain XMPP on the server anyway if you want more control. 

When Praveen told me the idea of Snikket a few days ago, I liked the idea and so I wanted to try and self-host it.

### Setting up on the server

[Sahil](https://sahilister.in) and I first tried to get Snikket running on a server with nginx running on it, but we failed to do so. Snikket's guide assumes that there is no reverse proxy running on the server. Then I thought that we should try deploying Snikket on a server in which there is no other service running. Sahil allowed access to a server that he was abandoning anyway. He gave a fresh Debian installed on the server. After that, we followed the [Snikket self-hosting guide](https://snikket.org/service/quickstart/) which was very simple and within a few minutes, the server was up and running.  

### Experience as a user

I created an admin invite link for myself. Then I scanned the QR code in the invite link using the Snikket app. The app asked me to set a username and password, after which my account got created and ready to go. Every user on Snikket needs an invite to join. The admin creates an invite link and shares with the person. The link gives various ways of joining and links to app stores to download the app as well. Then I created an invite link for Sahil. He joined using the Conversations app, which shows that you can use any xmpp app to accept the invite and use your account. we tried texting, file sharing, audio/video calls and it was working very well. Snikket gave 95% [compliance](https://compliance.conversations.im/) to my server out of the box, which usually takes a lot of work in plain XMPP, but Snikket made it easy. On this occassion, the founder of Snikket project, Matt replied to me on Mastodon reminding us not to take compliance measure too seriously: 

<iframe src="https://mastodon.technology/@mattj/107654001349079664/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe>

While generating the invite link, the admin can choose the circle of the person they are inviting. People joining the same circle are automatically added as contacts. They don't need to add manually. For example, you might have a circle called Family. So, everyone in the Family circle will know each other. You might have a different circle called 'College' and so on.

What I like about the Snikket is that it is very good quality out of the box. The audio/video call quality is superb, file sharing works like a breeze. 

### Further goals

The next step is to configure Snikket on a server running a reverse proxy like nginx. Me and Sahil tried but didn't get success. If that is successful, I can run the server in long-term. For now, it is only experimental and temporary.

Goodbye for now.

### Update on May 15 2022

After a week of writing this post, I could set up Snikket on nginx. It's been almost 5 months now and the service is very smooth. I only had to update once and that was very easy. I faced no issues as of now on the server side. The uptime has been 100%.

I found Snikket project chatroom as very helping and welcoming. They try their best to help newbies in self-hosting, which usually takes a lot of patience.
