---
title: "First Impressions of Albania"
date: 2022-07-06
draft: false
type: "post"
showTableOfContents: true
tags: ["travel", "albania", "europe"]
---
I landed in Tirana, Albania today, which is a country in the balkans region of the Europe.

My first impressions are:



- Tirana is neat and clean city with beautiful landscapes surrounding it.

<figure>
<img src="/images/tirana-1.jpg">
<figcaption>Beautiful views at the Tirana airport just after landing.</figcaption>
</figure>

<figure>
<img src="/images/tirana-2.jpg">
<figcaption>Tirana International Airport.</figcaption>
</figure>

<figure>
<img src="/images/tirana-3.jpg">
<figcaption>Taxis outside Tirana International Airport.</figcaption>
</figure>

<figure>
<img src="/images/tirana-4.jpg">
<figcaption>Beautiful views on the way from Tirana airport to city center.</figcaption>
</figure>

- Locals are very patient and hospitable.

- Streets are not very crowded and footpaths are wide.

<figure>
<img src="/images/tirana-5.jpg">
<figcaption>Wide footpaths in Tirana.</figcaption>
</figure>

<figure>
<img src="/images/tirana-6.jpg">
<figcaption>Tree-lined sidewalks in Tirana.</figcaption>
</figure>

<figure>
<img src="/images/tirana-7.jpg">
<figcaption>A bus stop in Tirana.</figcaption>
</figure>

- The city has bars and cafes all around the place.

<figure>
<img src="/images/tirana-8.jpg">
<figcaption>A bus stop in Tirana.</figcaption>
</figure>

- It is a well planned city.

- It is cheaper than most of the places in Europe.

My first impression is that this is a very beautiful and hospitable place. Although, people who don’t eat meat can have some difficulties in finding good vegetarian food.

I hope to have fun in this Albania trip.
