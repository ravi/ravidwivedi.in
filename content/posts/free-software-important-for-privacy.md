---
title: "Why Free Software and decentralization are necessary for privacy"
date: 2021-10-26
draft: false
type: "post"
showTableOfContents: true
---

[Free Software](/posts/free-sw) means software which respects user's freedom. It does not mean that the users get software without paying or for free of cost. Free Software is a matter of liberty not price. I sometimes call it swatantra/mukt software to clarify this point. 

Precisely, Free Software gives users the following freedoms: 

- Freedom 0: Freedom to run the software; 

- Freedom 1: Freedom to study and modify the software. Users must have the source code of the software to exercise this freedom; 

- Freedom 2: Freedom to share the software;

- Freedom 3: Freedom to share your modified versions; 

A software which lacks any of these freedoms is called nonfree/proprietary software. Most of the well-known [proprietary software are malware](https://gnu.org/malware).

If we do not have the source code of the software, we cannot inspect it for whether it has malicious functionalities. In particular, we cannot inspect whether the software has spyware or not. Even if we get the source code, we cannot remove the spyware unless we can modify it. In view of this note, freedom 1 is a precondition to user privacy. 

If users cannot share the software, then they need to inspect the software themselves to know whether software has some spying functionality or not. Sharing the software allows us to give a copy of software to someone who can inspect. For example, nonprogrammers can share a copy of the software with someone to get the software inspected for malicious functionalities, maybe, in exchange for a fee. When we don't know how to repair a fan, bicycle or a car and we give it to a mechanic who repairs for us. Similar is the case for software. 

Let's say someone removed a spyware from a software, then they can share their modifications with others if Freedom 3 is granted. If Freedom 3 is not granted, then everyone need to modify themselves which is a lot of redundant work. 

These freedoms give users collective control over the software. The collective control is necessary for users to get privacy. A nonfree software cannot be trusted for privacy. Please note that Free Software might not be sufficient for privacy. It is a precondition for privacy. 

For example, Ubuntu operating system is a Free Software and [it contained spyware in older versions](https://www.gnu.org/philosophy/ubuntu-spyware.en.html). With Free Software, users have a defence to remove those malfunctionalities. With proprietary software, there is no such chance. 

Another factor is decentralization. What I mean by decentralization is that network-based services allow self-hosting and [federation](/glossary/#federated-services). Please [read this article](https://fsci.in/blog/self-hosting-and-federation/) by FSCI for details on how decentralization in combination with Free Software and end-to-end encryption can give you privacy.

I will summarize the article for you here. Basically, when we use software installed in someone else's computers(called servers), like Google Docs, we lose control over our computing. This is because all communications, say, in case of Google Docs will go via Google's servers. Since, we are using Google's computers for our computing they can log our activities and put us in surveillance. If the server is in our control or a trusted one, then this is not a problem but services like Google Docs does not allow users to deploy it on their own server(even if it would it would not be advisable due to Google Docs being nonfree software). So we need the freedom to self-host, which means freedom to run our own servers. This does not mean we have to run our own server. Granting users the freedom to self-host gives rise to many service providers. Then we can choose one which we trust, or pay someone to deploy our server. 

Federation means two users using different service providers can communicate. This is required, otherwise when we switch the service provider, we need to make effort to switch every contact to a new service provider. Federation allows us to switch service provider without other contacts switching the provider. 

Example of free software powered federated systems are Matrix, XMPP, Mastodon. Searx search engine is an example where self-hosting is allowed but the concept of federation does not apply. 

To illustrate, we take an example of Matrix chat system. A user registered on service provider 1 can contact with users on service provider 2 on Matrix. All messages are end-to-end encrypted. And since the servers are under user's control, they can control the policies and what data is being collected.

The conclusion is: Free Software and decentralization are necessary for privacy. Proprietary Software and centralized services cannot be trusted for privacy. 
