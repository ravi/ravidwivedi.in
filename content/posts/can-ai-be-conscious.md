---
title: "Can AI be conscious?"
date: 2023-08-04T08:48:30+05:30
draft: true
type: 'post'
showTableOfContents: true
---
Can AI become conscious some day? Can it have feelings of its own? Can it have "awareness" like humans do? As much as we would like to believe 
