---
title: "Free Software Explained Simply"
date: 2021-11-25
draft: false
type: "post" 
showTableOfContents: true
---
The article was updated on 03 December 2021 to illustrate what source code means. 

Let's say your refrigerator has some problem, and it stopped working. You don't know how to repair it. There is a repair shop in your town. The components of the refrigerator are sealed and there are no components available in the market. Every company makes their own components which are not compatible with other company's refrigerators. That means you need to go to the same company to get it repaired, even though the mechanic in the shop near your house could have repaired it if it was not intentionally locked by the manufacturer. Unfortunately, it is weekend and the company services are not available on weekends.  

Then you book an appointment on Monday and wait for a few days for your turn. When your turn comes, the company takes the refrigerator and "diagnoses" it for a few days. Then it returns you a call saying that it will take ₹ 50,000 to repair it, and you can get a brand-new refrigerator for ₹ 60,000. So, you got frustrated and buy a new refrigerator. 

The story tells us the importance of freedom that we enjoy in our daily life. In the above story, if you had the freedom to see what's inside your device. But it didn't happen just because of the company making it intentionally locked and making sure that only they can repair all the products they sell. 

Think of the same scenario in software. Let's say you found a problem in the software you are working on. You are not a programmer, but other people who know programming can fix that problem. Every software is made of some human-readable code, which we call source code, which is then compiled by the computer. A software receives instructions from the code, and it does whatever the source code commands it to. You realized that the code on which the software is working is not available to you by the developer of that software. This leaves other programmers, except for the developer, in no position to fix the problem or even inspect what is wrong with the software. Similar to the refrigerator story above, the software source code is intentionally kept a secret from you and due to that restriction, you cannot get the problem fixed. 

You try to contact the developer and report the problem, but there is no response. Now you try to switch to some other software, but then those software lacks some other crucial features which the previous one had. Now you are thinking that if you had the code, then there was a possibility to fix that problem. In this situation, you have to wait for the developer to fix the problem or use some other software or make your own and that too from scratch just to fix a problem, which would have surely been less hassle than to write the software from scratch.

Also, as we discussed earlier, a software takes commands from the source code. That means, whoever has the source code, has the full control over that software. If the user does not have the source code, then the user does not control the software.  

#### What is source code? 

Well, what is source code? To illustrate, I would like to show you a simple code. Don't worry if you do not understand the code. I am only showing it to make my point. 

See the following HTML code: 

		<!DOCTYPE html>

		<html>

		<body>

		<h1 style="color:Red;">Hello World</h1>

		<p style="color:Blue;">I am Ravi</p>

		<p style="color:Purple;">What color is this text?</p> 

		</body>

		</html>

When I write these lines in a text editor, save this file as test.html and open test.html in a browser, it looks like the following image:

<img src="/images/html1.png" width="500" height="300">

		<!DOCTYPE html>

		<html>

		<body>

		<h1 style="color:Green;">Hello World</h1>

		<p style="color:Blue;">I am Ravi</p>

		<p style="color:Purple;">What color is this text?</p> 

		</body>

		</html>

<br>

Now, if I change the color from Red to Green in the heading of the code above, save the file and open in the browser again. The heading becomes green in color.  


See the below screenshot for this code: 

<img src="/images/html2.png" width="500" height="300">

<br>

Notice how change in code changes the color of the heading in the screenshots. You can do all sort of changes in the code and make the text whatever you like. Try copying the code in a text editor and save it as filename.html, where you can replace 'filename' by any name of your choice, and make changes in the text or color to play around and see it for yourself.  

The conclusion here is: The person/entity having source code of the software controls the software. For example, Microsoft has the source code of Microsoft Windows and therefore the software is in the control of Microsoft. The source code of Whatspp is with Facebook, so Facebook controls it. Users do not control the software if they do not have the source code.

<br> 

#### Defining Free Software 

With the above discussion, I wanted to illustrate that you cannot repair a software if you do not have the source code. You do not control the software if you do not have the source code. So, the user must have the source code so that they can inspect and modify it according to their needs. In the above example, if the source code were available with you, then you also require the freedom to share the software with people who know programming. It is analogous to you showing the refrigerator to a mechanic in the above example so that they can repair it.

Now let's say you have the source code of a software and the freedom to share it with others. There is a problem in that program you would like to fix. Someone who knows how to fix the problem, will modify the source code and give you their modified version so that your problem gets solved.

When the software guarantees all the above-mentioned freedoms and, in addition, the freedom to run the software for any purpose, we call it Free Software. Free Software is a matter of liberty, not price. So 'Free' in 'Free Software' means freedom and not price. It is also called Libre Software or Swatantra Software or Mukt Software to emphasize that it is about freedom and not price. Think of free as in free speech not as in free meal. 

The following poster gives the definition of Free Software: 

<figure>
<figcaption>  
<a title="Jeison Yehuda Amihud, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:4-freedoms-poster.png"><img width="256" alt="4-freedoms-poster" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/4-freedoms-poster.png/256px-4-freedoms-poster.png"></a>
<br>
Credits: Jeison Yehuda Amihud
<br>
Source: https://www.gnu.org/graphics/amihud-4-freedoms.html 
<br>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
 </figcaption>
</figure> 

#### Examples of Free Software

Check my [Free Software List](/free-software-list) for some examples of Free Software.  

An example is Scribus, which is a desktop publishing software used in publishing newspapers, magazines etc. Scribus only supported publishing in Latin languages. Oman government funded adding a feature to Scribus, due to which Arabic support was added. Due to this, Scribus got Malayalam support and many newspapers now use Scribus to publish. The full story is [here](/posts/scribus.html). It goes to show the value of freedom. Scribus is free software, which means it respects all the four freedoms mentioned in the poster. Source code being available, developers could add a feature and then used freedom 3 to share the modifications with others so that everyone, including nonprogrammers, benefitted from this. 

Another example is a free software named GNUKhata. Its development is funded by the Kerala government. Due to the freedom given by Free Software, GNUKhata is customizable. Due to its customizability, [it now supports many Indian languages](https://yourstory.com/smbstory/new-gst-friendly-open-source-accounting-software-for-craft-producers-and-micro-finance-groups/amp?utm_source=old-domain) and many features like GST-compliance were added. Since many proprietary software for accounting are very costly and cannot be bought but can only be used against a license fee paid annually or periodically, many small businesses cannot afford them. GNUKhata can be downloaded for free-of-cost and users, businesses do not have to pay any license fee or ask for permission to use it. 

Think of what the world would have been without Free Software. What would have happened if there was no Free Software in the above mentioned examples. 

<br>
Updated on Sunday 28 November 2021 to add suggestions by Praveen and Snehal.

<b>Credits: Thanks for Ravish, Praveen and Snehal for reviewing the article and giving suggestions.</b> 
